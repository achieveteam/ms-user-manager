import _ from 'lodash';
import Promise from 'bluebird';
import * as chai from 'chai';
import chaiPromised from 'chai-as-promised';
import uuid from 'uuid';
import { BadRequestError, ConflictError, NotFoundError } from '@badgeteam/service';
import { UserLocationAggregate } from '../code/userLocationAggregate';
import { AggregateTester } from '@badgeteam/service/testddd/aggregateTester';

let expect = chai.expect;
chai.use(chaiPromised);
chai.expect();

describe('UserLocationAggregate', () => {
    /** @type {AggregateTester} */
    let tester;

    const create = (...events) => {
        return new AggregateTester(UserLocationAggregate, events)
            .make()
            .then(t => tester = t)
            .then(() => tester);
    };

    describe('static', () => {
        expect(UserLocationAggregate.Scope).to.be.equal('userLocation');
    });

    describe('discover', () => {
        let payload;

        beforeEach(() => {
            payload = {
                userId: uuid(),
                badgeId: uuid(),
                location: { latitude: 40.0, longitude: 50.0 },
                evidence: 'evidence'
            };
        });

        it('Success: [created] event generated', () => {
            return create()
                .then(() => tester.execCommand('discover', payload))
                .then(() => tester.expectEventGenerated('created', { userId: payload.userId }));
        });

        it('Success: [created] event not generated if already exist', () => {
            return create({ name: 'created', data: { userId: payload.userId }})
                .then(() => tester.execCommand('discover', payload))
                .then(() => tester.expectEventNotGenerated('created'));
        });

        it('Success: [discovered] event generated', () => {
            return create()
                .then(() => tester.execCommand('discover', payload))
                .then(() => tester.expectEventGenerated('discovered', payload));
        });

        it('BadRequest: no userId', () => {
            return create()
                .then(() => expect(tester.execCommand('discover', _.omit(payload, 'userId')))
                    .to.be.rejectedWith(BadRequestError));
        });

        it('BadRequest: no badgeId', () => {
            return create()
                .then(() => expect(tester.execCommand('discover', _.omit(payload, 'badgeId')))
                    .to.be.rejectedWith(BadRequestError));
        });

        it('BadRequest: no location', () => {
            return create()
                .then(() => expect(tester.execCommand('discover', _.omit(payload, 'location')))
                    .to.be.rejectedWith(BadRequestError));
        });

        it('BadRequest: invalid location coordinates type', () => {
            _.set(payload, 'location', { lat: 'pif', lon: 'paf' });
            return create()
                .then(() => expect(tester.execCommand('discover', payload))
                    .to.be.rejectedWith(BadRequestError));
        });

        it('BadRequest: missed latitude', () => {
            _.unset(payload, 'location.latitude');
            return create()
                .then(() => expect(tester.execCommand('discover', payload))
                    .to.be.rejectedWith(BadRequestError));
        });

        it('BadRequest: missed longitude', () => {
            _.unset(payload, 'location.longitude');
            return create()
                .then(() => expect(tester.execCommand('discover', payload))
                    .to.be.rejectedWith(BadRequestError));
        });
    });
});

