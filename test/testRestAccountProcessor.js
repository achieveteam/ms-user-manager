import _ from 'lodash';
import Promise from 'bluebird';
import uuid from 'uuid';
import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';
import { Domain, UnauthorizedError, BadRequestError } from '@badgeteam/service';
import { RestAccountProcessor } from '../code/restAccountProcessor';
import { MockServiceBus } from '@badgeteam/service/mocks/mockServiceBus';

let expect = chai.expect;
chai.use(chaiAsPromised);
chai.expect();

describe('RestAccountProcessor', () => {
    /** @type {MockServiceBus} **/
    let bus = null;
    /** @type {RestAccountProcessor} **/
    let processor = null;
    /** @type {Domain} **/
    let userDomain = null;
    let authPayload = null;

    beforeEach(() => {
        bus = new MockServiceBus();
        userDomain = new Domain(bus, 'com.onbadge.user');

        processor = new RestAccountProcessor(userDomain);
        authPayload = {
            auth: {
                userId: uuid(),
                authorized: true
            }
        };
    });

    describe('commands', () => {
        describe('edit', () => {
            let userId = null;
            let aggregatePayload = null;
            let commandPayload = null;

            const expectedAggregatePayload = () => {
                expect(aggregatePayload).to.be.eql(_.merge({ aggregateId: commandPayload.auth.userId }, _.omit(commandPayload, 'auth')));
            };

            beforeEach(() => {
                aggregatePayload = null;

                commandPayload = _.merge({
                    nickname: 'nickname',
                    avatar: `img-${uuid()}.jpg`
                }, authPayload);

                bus.listen('com.onbadge.user', MockServiceBus.Command, 'update', (payload) => {
                    aggregatePayload = payload;
                    return Promise.resolve();
                });
            });

            it('Success: complete params', () => {
                return processor.processCommandEdit(commandPayload)
                    .then(() => {
                        expectedAggregatePayload();
                    });
            });

            it('Success: only nickname', () => {
                commandPayload = _.pick(commandPayload, 'nickname', 'auth');
                return processor.processCommandEdit(commandPayload)
                    .then(() => {
                        expectedAggregatePayload();
                    });
            });

            it('Success: only avatar', () => {
                commandPayload = _.pick(commandPayload, 'avatar', 'auth');
                return processor.processCommandEdit(commandPayload)
                    .then(() => {
                        expectedAggregatePayload();
                    });
            });

            it('Success: nothing to edit', () => {
                commandPayload = authPayload;
                return processor.processCommandEdit(commandPayload)
                    .then(() => {
                        expectedAggregatePayload();
                    });
            });

            it('Unauthorized: unauthorized user', () => {
                return expect(processor.processCommandEdit(_.set(commandPayload, 'auth.authorized', false))).to.be.rejectedWith(UnauthorizedError);
            });

            it('Unauthorized: empty auth', () => {
                return expect(processor.processCommandEdit(_.omit(commandPayload, 'auth'))).to.be.rejectedWith(UnauthorizedError);
            });
        });

        describe('delete', () => {
            let userId = null;
            let commandPayload = null;
            let aggregatePayload = null;

            const expectedAggregatePayload = () => {
                return {
                    aggregateId: userId
                };
            };

            beforeEach(() => {
                userId = uuid();

                commandPayload = _.merge({ }, authPayload);

                bus.listen('com.onbadge.user', MockServiceBus.Command, 'delete', (payload) => {
                    aggregatePayload = payload;
                    return Promise.resolve();
                });
            });

            it('Success: complete params', () => {
                return processor.processCommandDelete(commandPayload)
                    .then(() => {
                        expectedAggregatePayload();
                    });
            });

            it('Unauthorized: unauthorized user', () => {
                return expect(processor.processCommandDelete(_.set(commandPayload, 'auth.authorized', false))).to.be.rejectedWith(UnauthorizedError);
            });

            it('Unauthorized: empty auth', () => {
                return expect(processor.processCommandDelete(_.omit(commandPayload, 'auth'))).to.be.rejectedWith(UnauthorizedError);
            });
        });

        describe('blockUser', () => {
            let aggregatePayload, commandPayload;

            beforeEach(() => {
                aggregatePayload = null;

                commandPayload = _.merge({
                    userId: uuid.v4().toString(),
                    badgeId: uuid.v4().toString(),
                    target: 'badgeUnlock'
                }, authPayload);

                bus.listen('com.onbadge.user', MockServiceBus.Command, 'blockUser', (payload) => {
                    aggregatePayload = payload;
                    return Promise.resolve();
                });
            });

            it('Success: complete params', () => {
                return processor.processCommandBlockUser(commandPayload)
                    .then(() => expect(aggregatePayload).to.be.eql({
                        aggregateId: authPayload.auth.userId,
                        userId: commandPayload.userId,
                        badgeId: commandPayload.badgeId,
                        target: 'badgeUnlock'
                    }));
            });

            it('BadRequest: no userId', () => {
                _.unset(commandPayload, 'userId');
                return expect(processor.processCommandBlockUser(commandPayload))
                    .to.be.rejectedWith(BadRequestError);
            });

            it('BadRequest: no badgeId', () => {
                _.unset(commandPayload, 'badgeId');
                return expect(processor.processCommandBlockUser(commandPayload))
                    .to.be.rejectedWith(BadRequestError);
            });

            it('BadRequest: no target', () => {
                _.unset(commandPayload, 'target');
                return expect(processor.processCommandBlockUser(commandPayload))
                    .to.be.rejectedWith(BadRequestError);
            });

            it('BadRequest: wrong target', () => {
                _.set(commandPayload, 'target', 'badTarget');
                return expect(processor.processCommandBlockUser(commandPayload))
                    .to.be.rejectedWith(BadRequestError);
            });

            it('Unauthorized: no auth', () => {
                _.unset(commandPayload, 'auth');
                return expect(processor.processCommandBlockUser(commandPayload))
                    .to.be.rejectedWith(UnauthorizedError);
            });
        });
    });

    describe('queries', () => {
        describe('get', () => {
            let user = null;
            let projectionPayload = null;
            let queryPayload = null;

            const expectedProjectionPayload = () => {
                expect(projectionPayload).to.be.eql(_.omit(queryPayload, 'auth'));
            };

            beforeEach(() => {
                projectionPayload = null;
                user = uuid();

                queryPayload = _.merge({
                    userId: user
                }, authPayload);

                bus.listen('com.onbadge.user', MockServiceBus.Query, 'get', (payload) => {
                    projectionPayload = payload;
                    return Promise.resolve();
                });
            });

            it('Success: complete params', () => {
                return processor.processQueryGet(queryPayload)
                    .then(() => expectedProjectionPayload());
            });

            it('Success: no userId should return authorized user', () => {
                return processor.processQueryGet(_.omit(queryPayload, 'userId'))
                    .then(() => {
                        _.set(queryPayload, 'userId', queryPayload.auth.userId);
                        expectedProjectionPayload();
                    });
            });

            it('Unauthorized: unauthorized user can read other users profiles', () => {
                queryPayload = _.omit(queryPayload, 'auth');
                return expect(processor.processQueryGet(queryPayload))
                    .to.be.rejectedWith(UnauthorizedError);
            });

            it('Unauthorized: unauthorized user cannot read self profile', () => {
                _.set(queryPayload, 'auth.authorized', false);
                _.unset(queryPayload, 'userId');
                return expect(processor.processQueryGet(_.set(queryPayload, 'auth.authorized', false)))
                    .to.be.rejectedWith(UnauthorizedError);
            });
        });

        describe('getBlockedUsers', () => {
            let projectionPayload = null;
            let queryPayload = null;

            beforeEach(() => {
                projectionPayload = null;
                queryPayload = _.merge({ }, authPayload);

                bus.listen('com.onbadge.user', MockServiceBus.Query, 'getBlockedUsers', (payload) => {
                    projectionPayload = payload;
                    return Promise.resolve();
                });
            });

            it('Success: complete params', () => {
                return processor.processQueryGetBlockedUsers(queryPayload)
                    .then(() => expect(projectionPayload).to.be.eql({ userId: authPayload.auth.userId }));
            });

            it('Unauthorized: unauthorized user cannot read anything', () => {
                _.set(queryPayload, 'auth.authorized', false);
                return expect(processor.processQueryGetBlockedUsers(_.set(queryPayload, 'auth.authorized', false)))
                    .to.be.rejectedWith(UnauthorizedError);
            });

        });
    });
});
