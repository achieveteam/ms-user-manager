import _ from 'lodash';
import * as chai from 'chai';
import chaiPromised from 'chai-as-promised';
import chaiSubset from 'chai-subset';
import uuid from 'uuid';
import util from 'util';
import { UberUtils, BadRequestError, NotFoundError } from '@badgeteam/service';
import { ProjectionTester } from '@badgeteam/service/testddd/projectionTester';
import { BadgeUnlockCommentsProjection } from '../code/badgeUnlockCommentsProjection';
import { UsersProjectionComponent } from '../code/usersProjectionComponent';

let expect = chai.expect;
chai.use(chaiPromised);
chai.use(chaiSubset);

describe('BadgeUnlockCommentsProjection', () => {
    /** @type {ProjectionTester} */
    let tester = null;
    let unlockerId = null, userId = null, badgeId = null;
    const userNickname = 'John Smith', userAvatar = 'image.jpg';

    const event = (name, aggregateId, data) => ({
        name,
        aggregateId,
        date: new Date(),
        eventData: data
    });

    const createCommentEvents = (content = 'message', commentatorId = userId) => {
        const commentId = uuid();
        return {
            commentId,
            badgeUnlockCommented: event('comment.badgeUnlockCommented', commentId, {
                userId: commentatorId, badgeId, unlockerId, content
            }),
            badgeUnlockCommentEdited: event('comment.badgeUnlockCommentEdited', commentId, {
                userId: commentatorId, badgeId, unlockerId, content: 'edited message'
            }),
            badgeUnlockCommentDeleted: event('comment.badgeUnlockCommentDeleted', commentId, {
                userId: commentatorId, badgeId, unlockerId
            })
        }
    };

    const createProjection = (...events)  => {
        return new ProjectionTester(BadgeUnlockCommentsProjection)
            .make()
            .then(t => {
                tester = t;

                const usersComponent = tester.getComponent(UsersProjectionComponent);

                usersComponent.getUser = (id) => {
                    if (_.isEqual(userId, id))
                        return Promise.resolve({
                            id: userId,
                            nickname: userNickname,
                            res_avatar: userAvatar
                        });
                    return Promise.resolve({});
                };

                return tester.sendEvents(events);
            });
    };

    beforeEach(() => {
        unlockerId = uuid();
        userId = uuid();
        badgeId = uuid();
    });

    describe('query.getBadgeUnlockComments', () => {
        const getBadgeUnlockComments = (badgeId, unlockerId) => {
            return tester.execQuery('comment.getBadgeUnlockComments', { badgeId, unlockerId });
        };

        it('single comment', () => {
            const commentEvents = createCommentEvents();
            return createProjection(commentEvents.badgeUnlockCommented)
                .then(() => getBadgeUnlockComments(badgeId, unlockerId))
                .then(result => expect(result).to.containSubset([{
                    id: commentEvents.commentId,
                    creationDate: commentEvents.badgeUnlockCommented.date.toJSON(),
                    editDate: commentEvents.badgeUnlockCommented.date.toJSON(),
                    user: {
                        id: userId,
                        nickname: userNickname,
                        res_avatar: userAvatar
                    },
                    content: commentEvents.badgeUnlockCommented.eventData.content
                }]));
        });

        it('multiple comments from one user', () => {
            const commentEvents1 = createCommentEvents('An');
            const commentEvents2 = createCommentEvents('apple');
            return createProjection(
                    commentEvents1.badgeUnlockCommented,
                    commentEvents2.badgeUnlockCommented
                )
                .then(() => getBadgeUnlockComments(badgeId, unlockerId))
                .then(result => expect(result).to.containSubset([{
                    id: commentEvents1.commentId,
                    user: {
                        id: userId,
                        nickname: userNickname,
                        res_avatar: userAvatar
                    },
                    content: 'An'
                }, {
                    id: commentEvents2.commentId,
                    user: {
                        id: userId,
                        nickname: userNickname,
                        res_avatar: userAvatar
                    },
                    content: 'apple'
                }]));
        });

        it('comment was edited', () => {
            const commentEvents = createCommentEvents();
            return createProjection(commentEvents.badgeUnlockCommented, commentEvents.badgeUnlockCommentEdited)
                .then(() => getBadgeUnlockComments(badgeId, unlockerId))
                .then(result => expect(result).to.containSubset([{
                    id: commentEvents.commentId,
                    creationDate: commentEvents.badgeUnlockCommented.date.toJSON(),
                    editDate: commentEvents.badgeUnlockCommentEdited.date.toJSON(),
                    content: commentEvents.badgeUnlockCommentEdited.eventData.content
                }]));
        });

        it('comment was deleted', () => {
            const commentEvents = createCommentEvents();
            return createProjection(commentEvents.badgeUnlockCommented, commentEvents.badgeUnlockCommentDeleted)
                .then(() => getBadgeUnlockComments(badgeId, unlockerId))
                .then(result => expect(result).to.be.empty);
        });
    });
});


