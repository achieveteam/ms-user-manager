import _ from 'lodash';
import uuid from 'uuid';
import * as chai from 'chai';
import { expect } from 'chai';
import chaiPromised from 'chai-as-promised';
import { BadRequestError, ConflictError, NotFoundError } from '@badgeteam/service';
import { AggregateTester } from '@badgeteam/service/testddd/aggregateTester';
import { CommentAggregate } from '../code/commentAggregate';

chai.use(chaiPromised);

describe('CommentAggregate', () => {
    /** @type {AggregateTester} */
    let tester = null;
    let aggregateId = null;
    let commentBadgeUnlockPayload = null;

    /**
     * @returns {Promise<AggregateTester>}
     */
    const emptyComment = (...events) => {
        return new AggregateTester(CommentAggregate, events)
            .make()
            .then(t => tester = t)
            .then(() => tester);
    };

    /**
     * @returns {Promise<AggregateTester>}
     */
    const commentBadgeUnlock = (...events) => {
        return emptyComment(...[{ name: 'badgeUnlockCommented', data: commentBadgeUnlockPayload }].concat(events));
    };

    beforeEach(() => {
        aggregateId = uuid();
        commentBadgeUnlockPayload = {
            userId: uuid(),
            unlockerId: uuid(),
            badgeId: uuid(),
            content: 'comment content'
        };
    });

    describe('static', () => {
        it('scope', () => {
            expect(CommentAggregate.Scope).to.be.equal('comment');
        });
    });

    describe('commentBadgeUnlock', () => {
        it('Success: complete params', () => {
            return emptyComment()
                .then(() => tester.execCommand('commentBadgeUnlock', commentBadgeUnlockPayload))
                .then(() => tester.expectEventGenerated('badgeUnlockCommented', commentBadgeUnlockPayload))
        });

        it('BadRequest: no userId', () => {
            _.unset(commentBadgeUnlockPayload, 'userId');
            return emptyComment()
                .then(() => expect(tester.execCommand('commentBadgeUnlock', commentBadgeUnlockPayload))
                    .to.be.rejectedWith(BadRequestError));
        });

        it('BadRequest: no unlockerId', () => {
            _.unset(commentBadgeUnlockPayload, 'unlockerId');
            return emptyComment()
                .then(() => expect(tester.execCommand('commentBadgeUnlock', commentBadgeUnlockPayload))
                    .to.be.rejectedWith(BadRequestError));
        });

        it('BadRequest: no badgeId', () => {
            _.unset(commentBadgeUnlockPayload, 'badgeId');
            return emptyComment()
                .then(() => expect(tester.execCommand('commentBadgeUnlock', commentBadgeUnlockPayload))
                    .to.be.rejectedWith(BadRequestError));
        });

        it('BadRequest: no content', () => {
            _.unset(commentBadgeUnlockPayload, 'content');
            return emptyComment()
                .then(() => expect(tester.execCommand('commentBadgeUnlock', commentBadgeUnlockPayload))
                    .to.be.rejectedWith(BadRequestError));
        });

        it('Conflict: already exists', () => {
            return emptyComment()
                .then(() => tester.execCommand('commentBadgeUnlock', commentBadgeUnlockPayload))
                .then(() => expect(tester.execCommand('commentBadgeUnlock', commentBadgeUnlockPayload))
                    .to.be.rejectedWith(ConflictError));
        });
    });

    describe('edit', () => {
        let editPayload = null;

        beforeEach(() => {
            editPayload = {
                userId: commentBadgeUnlockPayload.userId,
                content: 'new comment content'
            };
        });

        it('Success: complete params', () => {
            return commentBadgeUnlock()
                .then(() => tester.execCommand('edit', editPayload))
                .then(() => tester.expectEventGenerated('badgeUnlockCommentEdited', {
                    userId: editPayload.userId,
                    unlockerId: commentBadgeUnlockPayload.unlockerId,
                    badgeId: commentBadgeUnlockPayload.badgeId,
                    content: editPayload.content
                }))
        });

        it('Success: no event if content was not changed', () => {
            _.set(editPayload, 'content', commentBadgeUnlockPayload.content);
            return commentBadgeUnlock()
                .then(() => tester.execCommand('edit', editPayload))
                .then(() => tester.expectEventNotGenerated('badgeUnlockCommentEdited'));
        });

        it('NotFound: comment not exist', () => {
            return emptyComment()
                .then(() => expect(tester.execCommand('edit', editPayload))
                    .to.be.rejectedWith(NotFoundError));
        });

        it('NotFound: comment not exist (deleted)', () => {
            return commentBadgeUnlock({ name: 'badgeUnlockCommentDeleted', data: { userId: commentBadgeUnlockPayload.userId }})
                .then(() => expect(tester.execCommand('edit', editPayload))
                    .to.be.rejectedWith(NotFoundError));
        });

        it('BadRequest: no userId', () => {
            _.unset(editPayload, 'userId');
            return commentBadgeUnlock()
                .then(() => expect(tester.execCommand('edit', editPayload))
                    .to.be.rejectedWith(BadRequestError));
        });

        it('BadRequest: no content', () => {
            _.unset(editPayload, 'content');
            return commentBadgeUnlock()
                .then(() => expect(tester.execCommand('edit', editPayload))
                    .to.be.rejectedWith(BadRequestError));
        });

        it('BadRequest: user not the owner', () => {
            _.set(editPayload, 'userId', uuid());
            return commentBadgeUnlock()
                .then(() => expect(tester.execCommand('edit', editPayload))
                    .to.be.rejectedWith(BadRequestError));
        });
    });

    describe('delete', () => {
        let deletePayload = null;

        beforeEach(() => {
            deletePayload = {
                userId: commentBadgeUnlockPayload.userId
            };
        });

        it('Success: complete params', () => {
            return commentBadgeUnlock()
                .then(() => tester.execCommand('delete', deletePayload))
                .then(() => tester.expectEventGenerated('badgeUnlockCommentDeleted', {
                    userId: deletePayload.userId,
                    unlockerId: commentBadgeUnlockPayload.unlockerId,
                    badgeId: commentBadgeUnlockPayload.badgeId
                }))
        });

        it('NotFound: comment not exist', () => {
            return emptyComment()
                .then(() => expect(tester.execCommand('delete', deletePayload))
                    .to.be.rejectedWith(NotFoundError));
        });

        it('NotFound: comment not exist (deleted)', () => {
            return commentBadgeUnlock({ name: 'badgeUnlockCommentDeleted', data: { userId: commentBadgeUnlockPayload.userId }})
                .then(() => expect(tester.execCommand('delete', deletePayload))
                    .to.be.rejectedWith(NotFoundError));
        });

        it('BadRequest: no userId', () => {
            _.unset(deletePayload, 'userId');
            return commentBadgeUnlock()
                .then(() => expect(tester.execCommand('delete', deletePayload))
                    .to.be.rejectedWith(BadRequestError));
        });

        it('BadRequest: user not the owner', () => {
            _.set(deletePayload, 'userId', uuid());
            return commentBadgeUnlock()
                .then(() => expect(tester.execCommand('delete', deletePayload))
                    .to.be.rejectedWith(BadRequestError));
        });
    });
});