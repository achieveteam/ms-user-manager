import _ from 'lodash';
import uuid from 'uuid';
import * as chai from 'chai';
import { expect } from 'chai';
import chaiPromised from 'chai-as-promised';
import { BadRequestError, ConflictError, NotFoundError } from '@badgeteam/service';
import { AggregateTester } from '@badgeteam/service/testddd/aggregateTester';
import { UserAggregate } from '../code/userAggregate';

chai.use(chaiPromised);

describe('UserAggregate', () => {
    let createPayload = null;
    let updatePayload = null;
    let deletePayload = null;
    let bindFriendsGroupPayload = null;

    /**
     * @returns {Promise<AggregateTester>}
     */
    const emptyUser = () => {
        return new AggregateTester(UserAggregate).make();
    };

    /**
     * @returns {Promise<AggregateTester>}
     */
    const createUser = (...additionalEvents) => {
        return new AggregateTester(UserAggregate, [ { name: 'created' , data: createPayload } ].concat(additionalEvents)).make();
    };

    beforeEach(() => {
        createPayload = {
            nickname: 'nickname',
            avatar: `img-${uuid()}`,
            email: 'email@mail.com'
        };
        deletePayload = { };
        updatePayload = {
            nickname: 'new nickname',
            avatar: `img-${uuid()}`,
            email: 'newmail@mail.com'
        };
        bindFriendsGroupPayload = {
            groupId: uuid()
        };
    });

    describe('static', () => {
        it('scope', () => {
            expect(UserAggregate.Scope).to.be.equal('user');
        });
    });

    describe('create', () => {
        it('Success: complete params', () => {
            return emptyUser()
                .then((tester) => {
                    return tester.execCommand('create', createPayload);
                })
                .then((r) => {
                    r.tester.expectEventGenerated('created', createPayload);
                });
        });

        it('Success: no avatar', () => {
            createPayload = _.omit(createPayload, 'avatar');
            return emptyUser()
                .then((tester) => {
                    return tester.execCommand('create', createPayload);
                })
                .then((r) => {
                    r.tester.expectEventGenerated('created', createPayload);
                });
        });

        it('Success: no email', () => {
            createPayload = _.omit(createPayload, 'email');
            return emptyUser()
                .then((tester) => {
                    return tester.execCommand('create', createPayload);
                })
                .then((r) => {
                    r.tester.expectEventGenerated('created', createPayload);
                });
        });

        it('BadRequest: no nickname', () => {
            createPayload = _.omit(createPayload, 'nickname');
            return emptyUser()
                .then((tester) => {
                    return expect(tester.execCommand('create', createPayload)).to.be.rejectedWith(BadRequestError);
                });
        });

        it('Conflict: already created', () => {
            return createUser()
                .then((tester) => {
                    return expect(tester.execCommand('create', createPayload)).to.be.rejectedWith(ConflictError);
                });
        });
    });

    describe('delete', () => {
        it('Success', () => {
            return createUser()
                .then((tester) => {
                    return tester.execCommand('delete', deletePayload);
                })
                .then((r) => {
                    r.tester.expectEventGenerated('deleted', {});
                });
        });

        it('NotFound: not exist', () => {
            return emptyUser()
                .then((tester) => {
                    return expect(tester.execCommand('delete', deletePayload)).to.be.rejectedWith(NotFoundError);
                });
        });

        it('NotFound: already deleted', () => {
            return createUser({ name: 'deleted' })
                .then((tester) => {
                    return expect(tester.execCommand('delete', deletePayload)).to.be.rejectedWith(NotFoundError);
                });
        });
    });

    describe('update', () => {
        it('Success: complete params', () => {
            return createUser()
                .then((tester) => {
                    return tester.execCommand('update', updatePayload);
                })
                .then((r) => {
                    r.tester.expectEventGenerated('nicknameChanged', { nickname: updatePayload.nickname });
                    r.tester.expectEventGenerated('avatarChanged', { avatar: updatePayload.avatar });
                    r.tester.expectEventGenerated('emailChanged', { email: updatePayload.email });
                });
        });

        it('Success: nickname only', () => {
            return createUser()
                .then((tester) => {
                    return tester.execCommand('update', _.pick(updatePayload, 'nickname'));
                })
                .then((r) => {
                    r.tester.expectEventGenerated('nicknameChanged', { nickname: updatePayload.nickname });
                    r.tester.expectEventNotGenerated('avatarChanged');
                    r.tester.expectEventNotGenerated('emailChanged');
                });
        });

        it('Success: email only', () => {
            return createUser()
                .then((tester) => {
                    return tester.execCommand('update', _.pick(updatePayload, 'email'));
                })
                .then((r) => {
                    r.tester.expectEventNotGenerated('nicknameChanged');
                    r.tester.expectEventNotGenerated('avatarChanged');
                    r.tester.expectEventGenerated('emailChanged', { email: updatePayload.email });
                });
        });

        it('Success: avatar only', () => {
            return createUser()
                .then((tester) => {
                    return tester.execCommand('update', _.pick(updatePayload, 'avatar'));
                })
                .then((r) => {
                    r.tester.expectEventNotGenerated('nicknameChanged');
                    r.tester.expectEventGenerated('avatarChanged', { avatar: updatePayload.avatar });
                    r.tester.expectEventNotGenerated('emailChanged');
                });
        });

        it('Success: nothing to update', () => {
            return createUser()
                .then((tester) => {
                    return tester.execCommand('update', {});
                })
                .then((r) => {
                    r.tester.expectEventNotGenerated('nicknameChanged');
                    r.tester.expectEventNotGenerated('avatarChanged');
                    r.tester.expectEventNotGenerated('emailChanged');
                });
        });
        
        it('Success: no duplicate events', () => {
            return createUser(
                    { name: 'nicknameChanged', data: _.pick(updatePayload, 'nickname') },
                    { name: 'avatarChanged', data: _.pick(updatePayload, 'avatar') },
                    { name: 'emailChanged', data: _.pick(updatePayload, 'email') }
                )
                .then((tester) => {
                    return tester.execCommand('update', updatePayload);
                })
                .then((r) => {
                    r.tester.expectEventNotGenerated('nicknameChanged');
                    r.tester.expectEventNotGenerated('avatarChanged');
                    r.tester.expectEventNotGenerated('emailChanged');
                });
        });

        it('NotFound: not exist', () => {
            return emptyUser()
                .then((tester) => {
                    return expect(tester.execCommand('update', updatePayload)).to.be.rejectedWith(NotFoundError);
                });
        });

        it('NotFound: deleted', () => {
            return createUser({ name: 'deleted' })
                .then((tester) => {
                    return expect(tester.execCommand('update', updatePayload)).to.be.rejectedWith(NotFoundError);
                });
        });
    });

    describe('bindFriendsGroup', () => {
        it('Success: complete params', () => {
            return createUser()
                .then(tester => tester.execCommand('bindFriendsGroup', bindFriendsGroupPayload))
                .then(r => r.tester.expectEventGenerated('friendsGroupBounded', {
                    groupId: bindFriendsGroupPayload.groupId
                }));
        });

        it('Success: no duplicate event', () => {
            return createUser({name: 'friendsGroupBounded', data: bindFriendsGroupPayload})
                .then(tester => tester.execCommand('bindFriendsGroup', bindFriendsGroupPayload))
                .then(r => r.tester.expectEventNotGenerated('friendsGroupBounded'));
        });

        it('Conflict: already bounded', () => {
            let conflictError = null;
            let boundedGroup = bindFriendsGroupPayload.groupId;
            return createUser({name: 'friendsGroupBounded', data: bindFriendsGroupPayload})
                .then(tester => {
                    return tester.execCommand('bindFriendsGroup', _.set(bindFriendsGroupPayload, 'groupId', uuid()))
                        .catch(ConflictError, (error) => {
                            conflictError = error;
                            expect(error.details.groupId).to.be.eql(boundedGroup);
                        })
                        .then(() => expect(conflictError).to.be.not.empty);
                });
        });

        it('BadRequest: no groupId', () => {
            return createUser()
                .then(tester =>
                    expect(tester.execCommand('bindFriendsGroup', _.omit(bindFriendsGroupPayload, 'groupId')))
                        .to.be.rejectedWith(BadRequestError));
        });

        it('NotFound: user does not created', () => {
            return emptyUser()
                .then(tester =>
                    expect(tester.execCommand('bindFriendsGroup', bindFriendsGroupPayload))
                        .to.be.rejectedWith(NotFoundError));
        });
    });

    describe('blockUser', () => {
        let payload = null;

        beforeEach(() => {
            payload = {
                target: 'badgeUnlock',
                userId: uuid.v4().toString(),
                badgeId: uuid.v4().toString()
            };
        });

        it('Success: complete params', () => {
            return createUser()
                .then((tester) => tester.execCommand('blockUser', payload))
                .then((r) => {
                    r.tester.expectEventGenerated('userBlocked', {
                        target: 'badgeUnlock',
                        userId: payload.userId,
                        badgeId: payload.badgeId
                    });
                });
        });

        it('Success: no duplicate events', () => {
            return createUser(
                    { name: 'userBlocked', data: payload },
                )
                .then((tester) => tester.execCommand('blockUser', payload))
                .then((r) => {
                    r.tester.expectEventNotGenerated('userBlocked');
                });
        });

        it('BadRequest: user cannot block itself', () => {
            return createUser()
                .then((tester) => {
                    _.set(payload, 'userId', tester.aggregateId);
                    return expect(tester.execCommand('blockUser', payload))
                        .to.be.rejectedWith(BadRequestError);
                });
        });

        it('BadRequest: no userId', () => {
            _.unset(payload, 'userId');
            return createUser()
                .then((tester) => expect(tester.execCommand('blockUser', payload))
                    .to.be.rejectedWith(BadRequestError));
        });

        it('BadRequest: no target', () => {
            _.unset(payload, 'target');
            return createUser()
                .then((tester) => expect(tester.execCommand('blockUser', payload))
                    .to.be.rejectedWith(BadRequestError));
        });

        it('BadRequest: no badgeId', () => {
            _.unset(payload, 'badgeId');
            return createUser()
                .then((tester) => expect(tester.execCommand('blockUser', payload))
                    .to.be.rejectedWith(BadRequestError));
        });

        it('NotFound: not exist', () => {
            return emptyUser()
                .then((tester) => expect(tester.execCommand('blockUser', payload))
                    .to.be.rejectedWith(NotFoundError));
        });

        it('NotFound: deleted', () => {
            return createUser({ name: 'deleted' })
                .then((tester) => expect(tester.execCommand('blockUser', payload))
                    .to.be.rejectedWith(NotFoundError));
        });
    });

    describe('banUser', () => {
        let payload = null;

        beforeEach(() => {
            payload = {
                adminId: uuid.v4().toString()
            };
        });

        it('Success: complete params', () => {
            return createUser()
                .then((tester) => tester.execCommand('banUser', payload))
                .then((r) => {
                    r.tester.expectEventGenerated('banned', {
                        adminId: payload.adminId
                    });
                });
        });

        it('Success: no duplicate events', () => {
            return createUser(
                    { name: 'banned', data: payload },
                )
                .then((tester) => tester.execCommand('banUser', payload))
                .then((r) => {
                    r.tester.expectEventNotGenerated('banned');
                });
        });

        it('BadRequest: user cannot ban itself', () => {
            return createUser()
                .then((tester) => {
                    _.set(payload, 'adminId', tester.aggregateId);
                    return expect(tester.execCommand('banUser', payload))
                        .to.be.rejectedWith(BadRequestError);
                });
        });

        it('BadRequest: no adminId', () => {
            _.unset(payload, 'adminId');
            return createUser()
                .then((tester) => expect(tester.execCommand('banUser', payload))
                    .to.be.rejectedWith(BadRequestError));
        });

        it('NotFound: not exist', () => {
            return emptyUser()
                .then((tester) => expect(tester.execCommand('banUser', payload))
                    .to.be.rejectedWith(NotFoundError));
        });

        it('NotFound: deleted', () => {
            return createUser({ name: 'deleted' })
                .then((tester) => expect(tester.execCommand('banUser', payload))
                    .to.be.rejectedWith(NotFoundError));
        });
    });
});