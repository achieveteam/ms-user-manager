import _ from 'lodash';
import Promise from 'bluebird';
import uuid from 'uuid';
import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';
import { Domain, UnauthorizedError, BadRequestError, ServiceUnavailableError } from '@badgeteam/service';
import { RestAdminProcessor } from '../code/restAdminProcessor';
import { MockServiceBus } from '@badgeteam/service/mocks/mockServiceBus';

let expect = chai.expect;
chai.use(chaiAsPromised);

describe('RestAdminProcessor', () => {
    /** @type {MockServiceBus} **/
    let bus = null;
    /** @type {RestAdminProcessor} **/
    let processor = null;
    /** @type {Domain} **/
    let userDomain = null;
    let authPayload = null;

    beforeEach(() => {
        bus = new MockServiceBus();
        userDomain = new Domain(bus, 'com.onbadge.user');

        processor = new RestAdminProcessor(userDomain);
        authPayload = {
            auth: {
                userId: uuid(),
                authorized: true,
                permissions: ['admin']
            }
        };
    });

    describe('commands', () => {
        describe('banUser', () => {
            let userId = null;
            let aggregatePayload = null;
            let commandPayload = null;

            beforeEach(() => {
                userId = uuid();
                aggregatePayload = null;

                commandPayload = _.merge({
                    userId
                }, authPayload);

                bus.listen('com.onbadge.user', MockServiceBus.Command, 'banUser', (payload) => {
                    aggregatePayload = payload;
                    return Promise.resolve();
                });
            });

            it('Success: complete params', () => {
                return processor.processCommandBanUser(commandPayload)
                    .then(() => {
                        expect(aggregatePayload).to.be.eql({
                            adminId: commandPayload.auth.userId,
                            aggregateId: userId
                        });
                    });
            });
            it('Unauthorized: unauthorized user', () => {
                return expect(processor.processCommandBanUser(_.set(commandPayload, 'auth.authorized', false)))
                    .to.be.rejectedWith(UnauthorizedError);
            });

            it('Unauthorized: empty auth', () => {
                return expect(processor.processCommandBanUser(_.omit(commandPayload, 'auth')))
                    .to.be.rejectedWith(UnauthorizedError);
            });

            it('ServiceUnavailable: no permissions', () => {
                _.unset(commandPayload.auth, 'permissions');
                return expect(processor.processCommandBanUser(commandPayload))
                    .to.be.rejectedWith(ServiceUnavailableError);
            });

            it('ServiceUnavailable: wrong permissions', () => {
                _.set(commandPayload, 'auth.permissions', ['user']);
                return expect(processor.processCommandBanUser(commandPayload))
                    .to.be.rejectedWith(ServiceUnavailableError);
            });
        });
    });
});
