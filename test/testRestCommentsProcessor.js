import _ from 'lodash';
import Promise from 'bluebird';
import uuid from 'uuid';
import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';
import { Domain, UnauthorizedError, BadRequestError, NotFoundError } from '@badgeteam/service';
import { RestCommentsProcessor } from '../code/restCommentsProcessor';
import { MockServiceBus } from '@badgeteam/service/mocks/mockServiceBus';

let expect = chai.expect;
chai.use(chaiAsPromised);

describe('RestCommentsProcessor', () => {
    /** @type {MockServiceBus} **/
    let bus = null;
    /** @type {RestCommentsProcessor} **/
    let processor = null;
    /** @type {Domain} **/
    let commentDomain = null;
    let badgeDomain = null;
    let authPayload = null;

    beforeEach(() => {
        bus = new MockServiceBus();
        commentDomain = new Domain(bus, 'com.onbadge.comment');
        badgeDomain = new Domain(bus, 'com.onbadge.badge');

        processor = new RestCommentsProcessor(commentDomain, badgeDomain);
        authPayload = {
            auth: {
                userId: uuid(),
                authorized: true
            }
        };
    });

    describe('commands', () => {
        describe('comment', () => {
            let aggregatePayload = null;
            let commandPayload = null;
            let badgeGetById = null;

            const expectedAggregatePayload = (id) => {
                expect(aggregatePayload).to.be.eql({
                    aggregateId: id,
                    badgeId: commandPayload.badgeId,
                    unlockerId: commandPayload.unlockerId,
                    userId: commandPayload.auth.userId,
                    content: commandPayload.content
                });
            };

            const execCommand = (payload = commandPayload) => {
                return processor.processCommandComment(payload);
            };

            beforeEach(() => {
                aggregatePayload = null;

                commandPayload = _.merge({
                    target: 'badgeUnlock',
                    badgeId: uuid(),
                    unlockerId: uuid(),
                    content: 'message'
                }, authPayload);

                badgeGetById = (payload) => {
                    if (payload.badgeId === commandPayload.badgeId)
                        return Promise.resolve({
                            isLocked: payload.userId !== commandPayload.unlockerId
                        });
                    return NotFoundError.promise();
                };

                bus.listen('com.onbadge.comment', MockServiceBus.Command, 'commentBadgeUnlock', (payload) => {
                    aggregatePayload = payload;
                    return Promise.resolve();
                });

                bus.listen('com.onbadge.badge', MockServiceBus.Query, 'getById', badgeGetById);
            });

            it('Success: complete params', () => {
                return execCommand()
                    .then(({ id }) => expectedAggregatePayload(id));
            });

            it('BadRequest: unknown target', () => {
                _.set(commandPayload, 'target', 'unknown');
                return expect(execCommand())
                    .to.be.rejectedWith(BadRequestError);
            });

            it('BadRequest: no target', () => {
                _.unset(commandPayload, 'target');
                return expect(execCommand())
                    .to.be.rejectedWith(BadRequestError);
            });

            it('BadRequest: no badgeId', () => {
                _.unset(commandPayload, 'badgeId');
                return expect(execCommand())
                    .to.be.rejectedWith(BadRequestError);
            });

            it('BadRequest: no unlockerId', () => {
                _.unset(commandPayload, 'unlockerId');
                return expect(execCommand())
                    .to.be.rejectedWith(BadRequestError);
            });

            it('BadRequest: no content', () => {
                _.unset(commandPayload, 'content');
                return expect(execCommand())
                    .to.be.rejectedWith(BadRequestError);
            });

            it('BadRequest: badge locked', () => {
                return expect(execCommand(_.set(_.cloneDeep(commandPayload), 'unlockerId', uuid.v4().toString())))
                    .to.be.rejectedWith(BadRequestError);
            });

            it('BadRequest: badge not exist', () => {
                return expect(execCommand(_.set(_.cloneDeep(commandPayload), 'badgeId', uuid.v4().toString())))
                    .to.be.rejectedWith(NotFoundError);
            });

            it('Unauthorized: unauthorized user', () => {
                _.set(commandPayload, 'auth.authorized', false);
                return expect(execCommand())
                    .to.be.rejectedWith(UnauthorizedError);
            });

            it('Unauthorized: empty auth', () => {
                _.unset(commandPayload, 'auth');
                return expect(execCommand())
                    .to.be.rejectedWith(UnauthorizedError);
            });
        });

        describe('edit', () => {
            let aggregatePayload = null;
            let commandPayload = null;

            const expectedAggregatePayload = () => {
                expect(aggregatePayload).to.be.eql({
                    aggregateId: commandPayload.commentId,
                    userId: commandPayload.auth.userId,
                    content: commandPayload.content
                });
            };

            const execCommand = () => {
                return processor.processCommandEdit(commandPayload);
            };

            beforeEach(() => {
                aggregatePayload = null;

                commandPayload = _.merge({
                    commentId: uuid(),
                    content: 'message'
                }, authPayload);

                bus.listen('com.onbadge.comment', MockServiceBus.Command, 'edit', (payload) => {
                    aggregatePayload = payload;
                    return Promise.resolve();
                });
            });

            it('Success: complete params', () => {
                return execCommand()
                    .then(() => expectedAggregatePayload());
            });

            it('BadRequest: no commentId', () => {
                _.unset(commandPayload, 'commentId');
                return expect(execCommand())
                    .to.be.rejectedWith(BadRequestError);
            });

            it('BadRequest: no content', () => {
                _.unset(commandPayload, 'content');
                return expect(execCommand())
                    .to.be.rejectedWith(BadRequestError);
            });

            it('Unauthorized: unauthorized user', () => {
                _.set(commandPayload, 'auth.authorized', false);
                return expect(execCommand())
                    .to.be.rejectedWith(UnauthorizedError);
            });

            it('Unauthorized: empty auth', () => {
                _.unset(commandPayload, 'auth');
                return expect(execCommand())
                    .to.be.rejectedWith(UnauthorizedError);
            });
        });

        describe('delete', () => {
            let aggregatePayload = null;
            let commandPayload = null;

            const expectedAggregatePayload = () => {
                expect(aggregatePayload).to.be.eql({
                    aggregateId: commandPayload.commentId,
                    userId: commandPayload.auth.userId
                });
            };

            const execCommand = () => {
                return processor.processCommandDelete(commandPayload);
            };

            beforeEach(() => {
                aggregatePayload = null;

                commandPayload = _.merge({
                    commentId: uuid()
                }, authPayload);

                bus.listen('com.onbadge.comment', MockServiceBus.Command, 'delete', (payload) => {
                    aggregatePayload = payload;
                    return Promise.resolve();
                });
            });

            it('Success: complete params', () => {
                return execCommand()
                    .then(() => expectedAggregatePayload());
            });

            it('BadRequest: no commentId', () => {
                _.unset(commandPayload, 'commentId');
                return expect(execCommand())
                    .to.be.rejectedWith(BadRequestError);
            });

            it('Unauthorized: unauthorized user', () => {
                _.set(commandPayload, 'auth.authorized', false);
                return expect(execCommand())
                    .to.be.rejectedWith(UnauthorizedError);
            });

            it('Unauthorized: empty auth', () => {
                _.unset(commandPayload, 'auth');
                return expect(execCommand())
                    .to.be.rejectedWith(UnauthorizedError);
            });
        });
    });

    describe('queries', () => {
        describe('get', () => {
            let queryPayload = null;
            let projectionPayload = null;

            const expectedProjectionPayload = () => {
                expect(projectionPayload).to.be.eql({
                    userId: queryPayload.auth.userId,
                    badgeId: queryPayload.badgeId,
                    unlockerId: queryPayload.unlockerId
                });
            };

            const execQuery = () => {
                return processor.processQueryGet(queryPayload);
            };

            beforeEach(() => {
                projectionPayload = null;

                queryPayload = _.merge({
                    target: 'badgeUnlock',
                    badgeId: uuid(),
                    unlockerId: uuid()
                }, authPayload);

                bus.listen('com.onbadge.comment', MockServiceBus.Query, 'getBadgeUnlockComments', (payload) => {
                    projectionPayload = payload;
                    return Promise.resolve();
                });
            });

            it('Success: complete params', () => {
                return execQuery()
                    .then(() => expectedProjectionPayload());
            });

            it('BadRequest: wrong target', () => {
                _.set(queryPayload, 'target', 'unknown');
                return expect(execQuery())
                    .to.be.rejectedWith(BadRequestError);
            });

            it('BadRequest: no target', () => {
                _.unset(queryPayload, 'target');
                return expect(execQuery())
                    .to.be.rejectedWith(BadRequestError);

            });

            it('BadRequest: no badgeId', () => {
                _.unset(queryPayload, 'badgeId');
                return expect(execQuery())
                    .to.be.rejectedWith(BadRequestError);
            });

            it('BadRequest: no unlockerId', () => {
                _.unset(queryPayload, 'unlockerId');
                return expect(execQuery())
                    .to.be.rejectedWith(BadRequestError);
            });

            it('Unauthorized: unauthorized user', () => {
                _.set(queryPayload, 'auth.authorized', false);
                return expect(execQuery())
                    .to.be.rejectedWith(UnauthorizedError);
            });

            it('Unauthorized: empty auth', () => {
                _.unset(queryPayload, 'auth');
                return expect(execQuery())
                    .to.be.rejectedWith(UnauthorizedError);
            });
        });
    });
});
