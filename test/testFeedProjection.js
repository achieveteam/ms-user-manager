import chai, { expect } from 'chai';
import chaiPromised from 'chai-as-promised';

import { ProjectionTester } from '@badgeteam/service/testddd/projectionTester';

import { FeedProjection } from '../feed/feedProjection';

chai.use(chaiPromised);


describe('FeedProjection', () => {
    const date = new Date("2014-02-10T10:50:42.389Z");
    const lateDate = new Date("2014-02-10T10:49:42.389Z");

    describe('badge created global', () => {
        it('without user', () => {
            return new ProjectionTester(FeedProjection, [
                    {
                        name: 'badge.created', aggregateId: 't_badge', date,
                        eventData: { creatorId: 't_creator', title: 't_title', image: 't_image', description: 't_description' }
                    }
                ]).make()
                .then((projection) => projection.execQuery('feed.get', { userId: null }))
                .then((feed) => {
                    expect(feed.length).to.be.equals(1);
                    const [{ id, ...item }] = feed;
                    expect(item).to.be.deep.equals({
                        type: 'created',
                        badge: {
                            cover: { color: '#1c88e3' },
                            description: 't_description',
                            id: 't_badge', res_image: 't_image', title: 't_title'
                        },
                        date, user: { id: 't_creator' }
                    });
                });
        });

        it('feed date slice', () => {
            return new ProjectionTester(FeedProjection, [
                    {
                        name: 'badge.created', aggregateId: 't_badge_late', lateDate,
                        eventData: { creatorId: 't_creator', title: 't_title', image: 't_image', description: 't_description' }
                    },
                    {
                        name: 'badge.created', aggregateId: 't_badge', date,
                        eventData: { creatorId: 't_creator', title: 't_title', image: 't_image', description: 't_description' }
                    },

                ]).make()
                .then((projection) => projection.execQuery('feed.get', { userId: null, date: date.toISOString() }))
                .then((feed) => {
                    expect(feed.length).to.be.equals(1);
                    const [{ id, ...item }] = feed;
                    expect(item).to.be.deep.equals({
                        type: 'created',
                        badge: {
                            cover: { color: '#1c88e3' },
                            description: 't_description',
                            id: 't_badge', res_image: 't_image', title: 't_title'
                        },
                        date, user: { id: 't_creator' }
                    });
                });
        });

        it('with badge update', () => {
            return new ProjectionTester(FeedProjection, [
                    {
                        name: 'badge.created', aggregateId: 't_badge', date,
                        eventData: { creatorId: 't_creator', title: 't_title'}
                    },
                    { name: 'badge.titleChanged', aggregateId: 't_badge', eventData: { title: 't_badge_title'} },
                    { name: 'badge.imageChanged', aggregateId: 't_badge', eventData: { image: 't_badge_image'} },
                    { name: 'badge.descriptionChanged', aggregateId: 't_badge', eventData: { description: 't_badge_description' } },
                    { name: 'badge.coverChanged', aggregateId: 't_badge', eventData: { cover: 't_badge_cover'} }
                ]).make()
                .then((projection) => projection.execQuery('feed.get', { userId: null }))
                .then((feed) => {
                    expect(feed.length).to.be.equals(1);
                    const [{ id, ...item }] = feed;
                    expect(item).to.be.deep.equals({
                        type: 'created',
                        badge: {
                            cover: 't_badge_cover',
                            id: 't_badge', res_image: 't_badge_image', title: 't_badge_title', description: 't_badge_description'
                        },
                        date, user: { id: 't_creator' }
                    });
                });
        });

        it('with user update', () => {
            return new ProjectionTester(FeedProjection, [
                    {
                        name: 'badge.created', aggregateId: 't_badge', date,
                        eventData: { creatorId: 't_creator' }
                    },
                    {
                        name: 'user.nicknameChanged', aggregateId: 't_creator',
                        eventData: { nickname: 't_nickname' }
                    },
                    {
                        eventData: { avatar: 't_avatar' },
                        name: 'user.avatarChanged', aggregateId: 't_creator'
                    }
                ]).make()
                .then((projection) => projection.execQuery('feed.get', { userId: null }))
                .then((feed) => {
                    expect(feed.length).to.be.equals(1);
                    const [{ id, ...item }] = feed;
                    expect(item).to.be.deep.equals({
                        type: 'created',
                        badge: {
                            cover: { color: '#1c88e3' },
                            id: 't_badge', res_image: '', title: 'No Title', description: 'No Description'
                        },
                        date, user: { id: 't_creator', nickname: 't_nickname', res_avatar: 't_avatar' }
                    });
                });
        });

        it('with user create', () => {
            return new ProjectionTester(FeedProjection, [
                    {
                        name: 'user.created', aggregateId: 't_creator',
                        eventData: { nickname: 't_nickname', avatar: 't_avatar' }
                    },
                    {
                        name: 'badge.created', aggregateId: 't_badge', date: new Date("2014-02-10T10:50:42.389Z"),
                        eventData: { creatorId: 't_creator', cover: 't_cover', description: 't_description', title: 't_title' }
                    }
                ]).make()
                .then((projection) => projection.execQuery('feed.get', { userId: null }))
                .then((feed) => {
                    expect(feed.length).to.be.equals(1);
                    const [{ id, ...item }] = feed;
                    expect(item).to.be.deep.equals({
                        type: 'created',
                        badge: {
                            cover: 't_cover',
                            id: 't_badge', res_image: '', title: 't_title', description: 't_description'
                        },
                        date, user: { id: 't_creator', nickname: 't_nickname', res_avatar: 't_avatar' }
                    });
                });
        });

        it('feed items should be removed on user delete', () => {
            return new ProjectionTester(FeedProjection, [
                {
                    name: 'user.created', aggregateId: 't_creator',
                    eventData: { nickname: 't_nickname', avatar: 't_avatar' }
                },
                {
                    name: 'badge.created', aggregateId: 't_badge', date: new Date("2014-02-10T10:50:42.389Z"),
                    eventData: { creatorId: 't_creator', title: 't_title'}
                },
                {
                    name: 'user.deleted', aggregateId: 't_creator'
                }
            ]).make()
                .then((projection) => projection.execQuery('feed.get', { userId: null }))
                .then((feed) => expect(feed.length).to.be.equals(0));
        });

        it('feed items should be removed on user ban', () => {
            return new ProjectionTester(FeedProjection, [
                {
                    name: 'user.created', aggregateId: 't_creator',
                    eventData: { nickname: 't_nickname', avatar: 't_avatar' }
                },
                {
                    name: 'badge.created', aggregateId: 't_badge', date: new Date("2014-02-10T10:50:42.389Z"),
                    eventData: { creatorId: 't_creator', title: 't_title'}
                },
                {
                    name: 'user.banned', aggregateId: 't_creator'
                }
            ]).make()
                .then((projection) => projection.execQuery('feed.get', { userId: null }))
                .then((feed) => expect(feed.length).to.be.equals(0));
        });
    });

    describe('badge unlocked global', () => {
        it('unlocked without badge and user', () => {
            return new ProjectionTester(FeedProjection, [
                    {
                        name: 'userBadges.unlocked', date,
                        eventData: { badgeId: 't_badge', userId: 't_user', match: {
                            unlockData: { type: 'offer', evidence: { image: 't_image', title: 't_title' }, adminId: 't_user' }
                        } }
                    }
                ]).make()
                .then((projection) => projection.execQuery('feed.get', { userId: null }))
                .then((feed) => {
                    expect(feed.length).to.be.equals(1);
                    const [{ id, ...item }] = feed;
                    expect(item).to.be.deep.equals({
                        type: 'unlocked',
                        user: { id: 't_user' },
                        badge: {
                            id: 't_badge'
                        },
                        date,
                        unlockData: {
                            type: 'offer',
                            admin: { id: 't_user' },
                            evidence: { res_image: 't_image', title: 't_title' },
                            comments: 0, likes: { value: 0, liked: false }
                        }
                    });
                });
        });

        it('unlocked with badge and user create', () => {
            return new ProjectionTester(FeedProjection, [
                    {
                        name: 'user.created', aggregateId: 't_user',
                        eventData: { nickname: 't_nickname', avatar: 't_avatar' }
                    },
                    {
                        name: 'badge.created', aggregateId: 't_badge',
                        eventData: { image: 't_badge_image', title: 't_badge_title', cover: 't_badge_cover', description: 't_description' }
                    },
                    {
                        name: 'userBadges.unlocked', date,
                        eventData: { badgeId: 't_badge', userId: 't_user', match: {
                            unlockData: { type: 'request', adminId: 't_user', evidence: { image: 't_image', title: 't_title' } }
                        } }
                    }
                ]).make()
                .then((projection) => projection.execQuery('feed.get', { userId: null }))
                .then((feed) => {
                    expect(feed.length).to.be.equals(2);
                    expect(feed.map(({ id, ...item }) => item)).to.be.deep.includes({
                        type: 'unlocked',
                        user: { id: 't_user', nickname: 't_nickname', res_avatar: 't_avatar' },
                        badge: {
                            id: 't_badge', title: 't_badge_title', res_image: 't_badge_image',
                            description: 't_description', cover: 't_badge_cover'
                        },
                        date,
                        unlockData: {
                            type: 'request',
                            admin: { id: 't_user', nickname: 't_nickname', res_avatar: 't_avatar' },
                            evidence: { res_image: 't_image', title: 't_title' },
                            comments: 0, likes: { value: 0, liked: false }
                        }
                    });
                });
        });

        it('unlocked with badge and user update', () => {
            return new ProjectionTester(FeedProjection, [
                    {
                        name: 'userBadges.unlocked', date,
                        eventData: { badgeId: 't_badge', userId: 't_user', match: {
                            unlockData: { evidence: { image: 't_image', title: 't_title' }, adminId: 't_user' }
                        } }
                    },
                    { name: 'user.nicknameChanged', aggregateId: 't_user', eventData: { nickname: 't_nickname' } },
                    { name: 'user.avatarChanged', aggregateId: 't_user', eventData: { avatar: 't_avatar' } },
                    { name: 'badge.titleChanged', aggregateId: 't_badge', eventData: { title: 't_badge_title'} },
                    { name: 'badge.imageChanged', aggregateId: 't_badge', eventData: { image: 't_badge_image'} },
                    { name: 'badge.descriptionChanged', aggregateId: 't_badge', eventData: { description: 't_badge_description'} },
                    { name: 'badge.coverChanged', aggregateId: 't_badge', eventData: { cover: 't_badge_cover'} }
                ]).make()
                .then((projection) => projection.execQuery('feed.get', { userId: null }))
                .then((feed) => {
                    expect(feed.length).to.be.equals(1);
                    const [{ id, ...item }] = feed;
                    expect(item).to.be.deep.equals({
                        type: 'unlocked',
                        user: { id: 't_user', nickname: 't_nickname', res_avatar: 't_avatar' },
                        badge: {
                            id: 't_badge', title: 't_badge_title', res_image: 't_badge_image',
                            description: 't_badge_description', cover: 't_badge_cover'
                        },
                        date,
                        unlockData: {
                            admin: { id: 't_user', nickname: 't_nickname', res_avatar: 't_avatar' },
                            evidence: { res_image: 't_image', title: 't_title' },
                            comments: 0, likes: { value: 0, liked: false }
                        }
                    });
                });
        });
    });

    describe('deleted badges', () => {
        it('deleted badge should be removed from the feed', () => {
            return new ProjectionTester(FeedProjection, [
                {
                    name: 'badge.created', aggregateId: 't_badge', date,
                    eventData: { creatorId: 't_creator', title: 't_title', image: 't_image', description: 't_description' }
                },
                {
                    name: 'badge.deleted', aggregateId: 't_badge', date,
                    eventData: { }
                }
            ]).make()
                .then((projection) => projection.execQuery('feed.get', { userId: null }))
                .then((feed) => {
                    expect(feed).to.be.empty;
                });
        });
    });

    describe('with followers', () => {
        describe('created/unlocked', () => {
            it('ok', () => {
                return new ProjectionTester(FeedProjection, [
                        {
                            name: 'userFriends.added',
                            eventData: { userId: 't_user', friendIds: ['t_user1', 't_user2'] }
                        },
                        {
                            name: 'userFriends.removed',
                            eventData: { userId: 't_user', friendIds: ['t_user2', 't_user3'] }
                        },
                        {
                            name: 'user.created', aggregateId: 't_user',
                            eventData: { nickname: 't_nickname', avatar: 't_avatar' }
                        },
                        {
                            name: 'badge.created', aggregateId: 't_badge', date,
                            eventData: {
                                creatorId: 't_user', image: 't_badge_image', title: 't_badge_title',
                                description: 't_description', cover: 't_badge_cover'
                            }
                        },
                        {
                            name: 'userBadges.unlocked', date,
                            eventData: { badgeId: 't_badge', userId: 't_user', match: { } }
                        }
                    ]).make()
                    .then((projection) =>
                        projection.execQuery('feed.get', { userId: 't_user' })
                            .then((feed) => {
                                expect(feed.length).to.be.equals(2);
                                expect(feed.map(({ id, unlockData, ...item }) => item)).to.be.deep.includes({
                                    type: 'unlocked',
                                    user: { id: 't_user', nickname: 't_nickname', res_avatar: 't_avatar' },
                                    badge: {
                                        id: 't_badge',
                                        title: 't_badge_title',
                                        res_image: 't_badge_image',
                                        description: 't_description',
                                        cover: 't_badge_cover'
                                    },
                                    date
                                });
                                expect(feed.map(({ id, ...item }) => item)).to.be.deep.includes({
                                    type: 'created',
                                    badge: {
                                        cover: 't_badge_cover', description: 't_description',
                                        id: 't_badge', res_image: 't_badge_image', title: 't_badge_title'
                                    },
                                    date,
                                    user: { id: 't_user', nickname: 't_nickname', res_avatar: 't_avatar' }
                                })
                            })
                            .then(() => projection)
                    )
                    .then((projection) =>
                        projection.execQuery('feed.get', { userId: 't_user1' })
                            .then((feed) => {
                                expect(feed.length).to.be.equals(2);
                                expect(feed.map(({ id, unlockData, ...item }) => item)).to.be.deep.includes({
                                    type: 'unlocked',
                                    user: { id: 't_user', nickname: 't_nickname', res_avatar: 't_avatar' },
                                    badge: {
                                        id: 't_badge', title: 't_badge_title', res_image: 't_badge_image',
                                        description: 't_description', cover: 't_badge_cover'
                                    },
                                    date
                                });
                                expect(feed.map(({ id, ...item }) => item)).to.be.deep.includes({
                                    type: 'created',
                                    badge: {
                                        cover: 't_badge_cover',
                                        id: 't_badge', res_image: 't_badge_image',
                                        description: 't_description', title: 't_badge_title'
                                    },
                                    date, user: { id: 't_user', nickname: 't_nickname', res_avatar: 't_avatar' }
                                });
                            })
                            .then(() => projection)
                    )
                    .then((projection) =>
                        projection.execQuery('feed.get', { userId: 't_user2' })
                            .then((feed) => {
                                expect(feed.length).to.be.equals(0);
                            })
                            .then(() => projection)
                    )
            });
        });
    });

    describe('with comments', () => {
        describe('created/unlocked', () => {
            it('commented', () => {
                return new ProjectionTester(FeedProjection, [
                        {
                            name: 'userFriends.added',
                            eventData: { userId: 't_user', friendIds: ['t_user1'] }
                        },
                        {
                            name: 'user.created', aggregateId: 't_user',
                            eventData: { nickname: 't_nickname', avatar: 't_avatar' }
                        },
                        {
                            name: 'badge.created', aggregateId: 't_badge', date,
                            eventData: {
                                creatorId: 't_user', image: 't_badge_image', title: 't_badge_title',
                                description: 't_description', cover: 't_badge_cover'
                            }
                        },
                        {
                            name: 'userBadges.unlocked', date,
                            eventData: { badgeId: 't_badge', userId: 't_user', match: { unlockData: { adminId: 't_admin' } } }
                        },
                        {
                            name: 'comment.badgeUnlockCommented',
                            eventData: { unlockerId: 't_user', badgeId: 't_badge' }
                        }
                    ]).make()
                    .then((projection) => projection.execQuery('feed.get', { userId: 't_user1' }))
                    .then((feed) => {
                        expect(feed.length).to.be.equals(2);
                        expect(feed.map(({ id, ...item }) => item)).to.be.deep.includes({
                            type: 'unlocked',
                            user: { id: 't_user', nickname: 't_nickname', res_avatar: 't_avatar' },
                            badge: {
                                id: 't_badge', title: 't_badge_title', res_image: 't_badge_image',
                                description: 't_description', cover: 't_badge_cover'
                            },
                            date,
                            unlockData: {
                                admin: { id: 't_admin' },
                                evidence: { res_image: '', title: 'No Title' },
                                comments: 1, likes: { value: 0, liked: false }
                            }
                        });
                        expect(feed.map(({ id, ...item }) => item)).to.be.deep.includes({
                            type: 'created',
                            badge: {
                                cover: 't_badge_cover', description: 't_description',
                                id: 't_badge', res_image: 't_badge_image', title: 't_badge_title'
                            },
                            date, user: { id: 't_user', nickname: 't_nickname', res_avatar: 't_avatar' }
                        });
                    });
            });

            it('uncommented', () => {
                return new ProjectionTester(FeedProjection, [
                        {
                            name: 'userFriends.added',
                            eventData: { userId: 't_user', friendIds: ['t_user1'] }
                        },
                        {
                            name: 'user.created', aggregateId: 't_user',
                            eventData: { nickname: 't_nickname', avatar: 't_avatar' }
                        },
                        {
                            name: 'badge.created', aggregateId: 't_badge', date,
                            eventData: {
                                creatorId: 't_user', image: 't_badge_image', title: 't_badge_title',
                                description: 't_description', cover: 't_badge_cover'
                            }
                        },
                        {
                            name: 'userBadges.unlocked', date,
                            eventData: { badgeId: 't_badge', userId: 't_user', match: { unlockData: { adminId: 't_admin' }} }
                        },
                        {
                            name: 'comment.badgeUnlockCommented',
                            eventData: { unlockerId: 't_user', badgeId: 't_badge' }
                        },
                        {
                            name: 'comment.badgeUnlockCommented',
                            eventData: { unlockerId: 't_user', badgeId: 't_badge' }
                        },
                        {
                            name: 'comment.badgeUnlockCommentDeleted',
                            eventData: { unlockerId: 't_user', badgeId: 't_badge' }
                        }
                    ]).make()
                    .then((projection) => projection.execQuery('feed.get', { userId: 't_user1' }))
                    .then((feed) => {
                        expect(feed.length).to.be.equals(2);
                        expect(feed.map(({ id, ...item }) => item)).to.be.deep.includes({
                            type: 'unlocked',
                            user: { id: 't_user', nickname: 't_nickname', res_avatar: 't_avatar' },
                            badge: {
                                id: 't_badge', title: 't_badge_title', res_image: 't_badge_image',
                                description: 't_description', cover: 't_badge_cover'
                            },
                            date,
                            unlockData: {
                                admin: { id: 't_admin' },
                                evidence: { res_image: '', title: 'No Title' },
                                comments: 1, likes: { value: 0, liked: false }
                            }
                        });
                        expect(feed.map(({ id, ...item }) => item)).to.be.deep.includes({
                            type: 'created',
                            badge: {
                                cover: 't_badge_cover', description: 't_description',
                                id: 't_badge', res_image: 't_badge_image', title: 't_badge_title'
                            },
                            date, user: { id: 't_user', nickname: 't_nickname', res_avatar: 't_avatar' }
                        });
                    });
            });
        });
    });

    describe('with likes', () => {
        describe('created/unlocked', () => {
            it('liked', () => {
                return new ProjectionTester(FeedProjection, [
                        {
                            name: 'userFriends.added',
                            eventData: { userId: 't_user', friendIds: ['t_user1'] }
                        },
                        {
                            name: 'user.created', aggregateId: 't_user',
                            eventData: { nickname: 't_nickname', avatar: 't_avatar' }
                        },
                        {
                            name: 'badge.created', aggregateId: 't_badge', date,
                            eventData: {
                                creatorId: 't_user', image: 't_badge_image', title: 't_badge_title',
                                description: 't_description', cover: 't_badge_cover'
                            }
                        },
                        {
                            name: 'userBadges.unlocked', date,
                            eventData: { badgeId: 't_badge', userId: 't_user', match: { unlockData: { adminId: 't_admin' } } }
                        },
                        {
                            name: 'userBadges.unlockedBadgeLiked',
                            eventData: { fellowId: 't_user1', badgeId: 't_badge', userId: 't_user' }
                        }
                    ]).make()
                    .then((projection) =>
                        projection.execQuery('feed.get', { authUserId: 't_user1', userId: 't_user1' })
                            .then((feed) => {
                                expect(feed.length).to.be.equals(2);
                                expect(feed.map(({ id, ...item }) => item)).to.be.deep.includes({
                                    type: 'unlocked',
                                    user: { id: 't_user', nickname: 't_nickname', res_avatar: 't_avatar' },
                                    badge: {
                                        id: 't_badge', title: 't_badge_title', res_image: 't_badge_image',
                                        description: 't_description', cover: 't_badge_cover'
                                    },
                                    date,
                                    unlockData: {
                                        admin: { id: 't_admin' },
                                        evidence: { res_image: '', title: 'No Title' },
                                        comments: 0, likes: { value: 1, liked: true }
                                    }
                                });
                                expect(feed.map(({ id, ...item }) => item)).to.be.deep.includes({
                                    type: 'created',
                                    badge: {
                                        cover: 't_badge_cover', description: 't_description',
                                        id: 't_badge', res_image: 't_badge_image', title: 't_badge_title'
                                    },
                                    date, user: { id: 't_user', nickname: 't_nickname', res_avatar: 't_avatar' }
                                });
                            })
                            .then(() => projection)
                    )
                    .then((projection) =>
                        projection.execQuery('feed.get', { authUserId: 't_user', userId: 't_user1' })
                            .then((feed) => {
                                expect(feed.length).to.be.equals(2);
                                expect(feed.map(({ id, ...item }) => item)).to.be.deep.includes({
                                    type: 'unlocked',
                                    user: { id: 't_user', nickname: 't_nickname', res_avatar: 't_avatar' },
                                    badge: {
                                        id: 't_badge', title: 't_badge_title', res_image: 't_badge_image',
                                        description: 't_description', cover: 't_badge_cover'
                                    },
                                    date,
                                    unlockData: {
                                        admin: { id: 't_admin' },
                                        evidence: { res_image: '', title: 'No Title' },
                                        comments: 0, likes: { value: 1, liked: false }
                                    }
                                });
                                expect(feed.map(({ id, ...item }) => item)).to.be.deep.includes({
                                    type: 'created',
                                    badge: {
                                        cover: 't_badge_cover', description: 't_description',
                                        id: 't_badge', res_image: 't_badge_image', title: 't_badge_title'
                                    },
                                    date, user: { id: 't_user', nickname: 't_nickname', res_avatar: 't_avatar' }
                                });
                            })
                    );
            });

            it('double liked', () => {
                return new ProjectionTester(FeedProjection, [
                        {
                            name: 'userFriends.added',
                            eventData: { userId: 't_user', friendIds: ['t_user1'] }
                        },
                        {
                            name: 'user.created', aggregateId: 't_user',
                            eventData: { nickname: 't_nickname', avatar: 't_avatar' }
                        },
                        {
                            name: 'badge.created', aggregateId: 't_badge', date,
                            eventData: {
                                creatorId: 't_user', image: 't_badge_image', title: 't_badge_title',
                                description: 't_description', cover: 't_badge_cover'
                            }
                        },
                        {
                            name: 'userBadges.unlocked', date,
                            eventData: { badgeId: 't_badge', userId: 't_user', match: { unlockData: { adminId: 't_admin' } } }
                        },
                        {
                            name: 'userBadges.unlockedBadgeLiked',
                            eventData: { fellowId: 't_user1', badgeId: 't_badge', userId: 't_user' }
                        },
                        {
                            name: 'userBadges.unlockedBadgeLiked',
                            eventData: { fellowId: 't_user1', badgeId: 't_badge', userId: 't_user' }
                        }
                    ]).make()
                    .then((projection) => projection.execQuery('feed.get', { authUserId: 't_user1', userId: 't_user1' }))
                    .then((feed) => {
                        expect(feed.length).to.be.equals(2);
                        expect(feed.map(({ id, ...item }) => item)).to.be.deep.includes({
                            type: 'unlocked',
                            user: { id: 't_user', nickname: 't_nickname', res_avatar: 't_avatar' },
                            badge: {
                                id: 't_badge', title: 't_badge_title', res_image: 't_badge_image',
                                description: 't_description', cover: 't_badge_cover'
                            },
                            date,
                            unlockData: {
                                admin: { id: 't_admin' },
                                evidence: { res_image: '', title: 'No Title' },
                                comments: 0, likes: { value: 1, liked: true }
                            }
                        });
                    });
            });

            it('disliked', () => {
                return new ProjectionTester(FeedProjection, [
                        {
                            name: 'userFriends.added',
                            eventData: { userId: 't_user', friendIds: ['t_user1'] }
                        },
                        {
                            name: 'user.created', aggregateId: 't_user',
                            eventData: { nickname: 't_nickname', avatar: 't_avatar' }
                        },
                        {
                            name: 'badge.created', aggregateId: 't_badge', date,
                            eventData: {
                                creatorId: 't_user', image: 't_badge_image', title: 't_badge_title',
                                description: 't_description', cover: 't_badge_cover'
                            }
                        },
                        {
                            name: 'userBadges.unlocked', date,
                            eventData: { badgeId: 't_badge', userId: 't_user', match: { unlockData: { adminId: 't_admin' } } }
                        },
                        {
                            name: 'userBadges.unlockedBadgeLiked',
                            eventData: { fellowId: 't_user1', badgeId: 't_badge', userId: 't_user' }
                        },
                        {
                            name: 'userBadges.unlockedBadgeDisliked',
                            eventData: { fellowId: 't_user1', badgeId: 't_badge', userId: 't_user' }
                        }
                    ]).make()
                    .then((projection) =>
                        projection.execQuery('feed.get', { authUserId: 't_user1', userId: 't_user1' })
                            .then((feed) => {
                                expect(feed.length).to.be.equals(2);
                                expect(feed.map(({ id, ...item }) => item)).to.be.deep.includes({
                                    type: 'unlocked',
                                    user: { id: 't_user', nickname: 't_nickname', res_avatar: 't_avatar' },
                                    badge: {
                                        id: 't_badge', title: 't_badge_title', res_image: 't_badge_image',
                                        description: 't_description', cover: 't_badge_cover'
                                    },
                                    date,
                                    unlockData: {
                                        admin: { id: 't_admin' },
                                        evidence: { res_image: '', title: 'No Title' },
                                        comments: 0, likes: { value: 0, liked: false }
                                    }
                                });
                                expect(feed.map(({ id, ...item }) => item)).to.be.deep.includes({
                                    type: 'created',
                                    badge: {
                                        cover: 't_badge_cover', description: 't_description',
                                        id: 't_badge', res_image: 't_badge_image', title: 't_badge_title'
                                    },
                                    date, user: { id: 't_user', nickname: 't_nickname', res_avatar: 't_avatar' }
                                });
                            })
                            .then(() => projection)
                    )
                    .then((projection) =>
                        projection.execQuery('feed.get', { authUserId: 't_user', userId: 't_user1' })
                            .then((feed) => {
                                expect(feed.length).to.be.equals(2);
                                expect(feed.map(({ id, ...item }) => item)).to.be.deep.includes({
                                    type: 'unlocked',
                                    user: { id: 't_user', nickname: 't_nickname', res_avatar: 't_avatar' },
                                    badge: {
                                        id: 't_badge', title: 't_badge_title', res_image: 't_badge_image',
                                        description: 't_description', cover: 't_badge_cover'
                                    },
                                    date,
                                    unlockData: {
                                        admin: { id: 't_admin' },
                                        evidence: { res_image: '', title: 'No Title' },
                                        comments: 0, likes: { value: 0, liked: false }
                                    }
                                });
                                expect(feed.map(({ id, ...item }) => item)).to.be.deep.includes({
                                    type: 'created',
                                    badge: {
                                        cover: 't_badge_cover', description: 't_description',
                                        id: 't_badge', res_image: 't_badge_image', title: 't_badge_title'
                                    },
                                    date, user: { id: 't_user', nickname: 't_nickname', res_avatar: 't_avatar' }
                                });
                            })
                    );
            });

            it('double disliked', () => {
                return new ProjectionTester(FeedProjection, [
                        {
                            name: 'userFriends.added',
                            eventData: { userId: 't_user', friendIds: ['t_user1'] }
                        },
                        {
                            name: 'user.created', aggregateId: 't_user',
                            eventData: { nickname: 't_nickname', avatar: 't_avatar' }
                        },
                        {
                            name: 'badge.created', aggregateId: 't_badge', date,
                            eventData: {
                                creatorId: 't_user', image: 't_badge_image', title: 't_badge_title',
                                description: 't_description', cover: 't_badge_cover'
                            }
                        },
                        {
                            name: 'userBadges.unlocked', date,
                            eventData: { badgeId: 't_badge', userId: 't_user', match: { unlockData: { adminId: 't_admin' } } }
                        },
                        {
                            name: 'userBadges.unlockedBadgeLiked',
                            eventData: { fellowId: 't_user1', badgeId: 't_badge', userId: 't_user' }
                        },
                        {
                            name: 'userBadges.unlockedBadgeDisliked',
                            eventData: { fellowId: 't_user1', badgeId: 't_badge', userId: 't_user' }
                        },
                        {
                            name: 'userBadges.unlockedBadgeDisliked',
                            eventData: { fellowId: 't_user1', badgeId: 't_badge', userId: 't_user' }
                        }
                    ]).make()
                    .then((projection) => projection.execQuery('feed.get', { authUserId: 't_user1', userId: 't_user1' }))
                    .then((feed) => {
                        expect(feed.length).to.be.equals(2);
                        expect(feed.map(({ id, ...item }) => item)).to.be.deep.includes({
                            type: 'unlocked',
                            user: { id: 't_user', nickname: 't_nickname', res_avatar: 't_avatar' },
                            badge: {
                                id: 't_badge', title: 't_badge_title', res_image: 't_badge_image',
                                description: 't_description', cover: 't_badge_cover'
                            },
                            date,
                            unlockData: {
                                admin: { id: 't_admin' },
                                evidence: { res_image: '', title: 'No Title' },
                                comments: 0, likes: { value: 0, liked: false }
                            }
                        });
                        expect(feed.map(({ id, ...item }) => item)).to.be.deep.includes({
                            type: 'created',
                            badge: {
                                cover: 't_badge_cover', description: 't_description',
                                id: 't_badge', res_image: 't_badge_image', title: 't_badge_title'
                            },
                            date, user: { id: 't_user', nickname: 't_nickname', res_avatar: 't_avatar' }
                        });
                    });
            });
        });
    });

    describe('filter', () => {
        it('filter badgeUnlocked', () => {
            return new ProjectionTester(FeedProjection, [
                {
                    name: 'badge.created', aggregateId: 't_badge',
                    eventData: {
                        image: 't_badge_image',
                        title: 't_badge_title',
                        cover: 't_badge_cover',
                        description: 't_description'
                    }
                },
                {
                    name: 'userBadges.unlocked', date,
                    eventData: {
                        badgeId: 't_badge', userId: 't_user', match: {
                            unlockData: {
                                type: 'request',
                                adminId: 't_user',
                                evidence: { image: 't_image', title: 't_title' }
                            }
                        }
                    }
                }
            ]).make()
                .then((projection) => projection.execQuery('feed.get', { userId: null, filter: 'badgeUnlocked' }))
                .then((feed) => {
                    expect(feed.length).to.be.equals(1);
                    const [ { id, ...item } ] = feed;
                    expect(item).to.be.deep.equals({
                        type: 'unlocked',
                        user: { id: 't_user' },
                        badge: {
                            cover: 't_badge_cover',
                            id: 't_badge',
                            res_image: 't_badge_image',
                            title: 't_badge_title',
                            description: 't_description'
                        },
                        date,
                        unlockData: {
                            type: 'request',
                            admin: { id: 't_user' },
                            evidence: { res_image: 't_image', title: 't_title' },
                            comments: 0, likes: { value: 0, liked: false }
                        }
                    });
                });
        });

        it('filter badgeCreated', () => {
            return new ProjectionTester(FeedProjection, [
                {
                    name: 'user.created', aggregateId: 't_user',
                    eventData: { nickname: 't_nickname', avatar: 't_avatar' }
                },
                {
                    name: 'badge.created', aggregateId: 't_badge', date,
                    eventData: {
                        image: 't_badge_image',
                        title: 't_badge_title',
                        cover: 't_badge_cover',
                        description: 't_description',
                        creatorId: 't_user'
                    }
                },
                {
                    name: 'userBadges.unlocked', lateDate,
                    eventData: {
                        badgeId: 't_badge', userId: 't_user', match: {
                            unlockData: {
                                type: 'request',
                                adminId: 't_user',
                                evidence: { image: 't_image', title: 't_title' }
                            }
                        }
                    }
                }
            ]).make()
                .then((projection) => projection.execQuery('feed.get', { userId: null, filter: 'badgeCreated' }))
                .then((feed) => {
                    expect(feed.length).to.be.equals(1);
                    const [ { id, ...item } ] = feed;
                    expect(item).to.be.deep.equals({
                        type: 'created',
                        date,
                        user: {
                            id: 't_user',
                            nickname: 't_nickname',
                            res_avatar: 't_avatar'
                        },
                        badge: {
                            cover: 't_badge_cover',
                            id: 't_badge',
                            res_image: 't_badge_image',
                            title: 't_badge_title',
                            description: 't_description'
                        },
                    });
                });
        });

        it('mixed items and limit', () => {
            const timeline = [
                new Date("2014-02-10T10:49:42.389Z"),
                new Date("2014-02-10T10:50:42.389Z"),
                new Date("2014-02-10T10:51:42.389Z"),
                new Date("2014-02-10T10:52:42.389Z"),
            ];

            return new ProjectionTester(FeedProjection, [
                {
                    name: 'user.created', aggregateId: 't_user',
                    eventData: { nickname: 't_nickname', avatar: 't_avatar' }
                },
                {
                    name: 'badge.created', aggregateId: 't_badge_1', date: timeline[0],
                    eventData: {
                        image: 't_badge_image',
                        title: 't_badge_title',
                        cover: 't_badge_cover',
                        description: 't_description',
                        creatorId: 't_user'
                    }
                },
                {
                    name: 'userBadges.unlocked', date: timeline[1],
                    eventData: {
                        badgeId: 't_badge', userId: 't_user', match: {
                            unlockData: {
                                type: 'request',
                                adminId: 't_user',
                                evidence: { image: 't_image', title: 't_title' }
                            }
                        }
                    }
                },
                {
                    name: 'badge.created', aggregateId: 't_badge_2', date: timeline[2],
                    eventData: {
                        image: 't_badge_image_1',
                        title: 't_badge_title_1',
                        cover: 't_badge_cover_1',
                        description: 't_description_1',
                        creatorId: 't_user'
                    }
                },
                {
                    name: 'badge.created', aggregateId: 't_badge_3', date: timeline[3],
                    eventData: {
                        image: 't_badge_image_1',
                        title: 't_badge_title_1',
                        cover: 't_badge_cover_1',
                        description: 't_description_1',
                        creatorId: 't_user'
                    }
                },
                {
                    name: 'userBadges.unlocked', date: timeline[4],
                    eventData: {
                        badgeId: 't_badge_2', userId: 't_user', match: {
                            unlockData: {
                                type: 'request',
                                adminId: 't_user_1',
                                evidence: { image: 't_image', title: 't_title' }
                            }
                        }
                    }
                }
            ]).make()
                .then((projection) => projection.execQuery('feed.get', { userId: null, filter: 'badgeCreated', limit: 2 }))
                .then((feed) => {
                    expect(feed.length).to.be.equals(2);
                });
        });
    });
});