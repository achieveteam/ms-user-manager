import _ from 'lodash';
import * as chai from 'chai';
import chaiPromised from 'chai-as-promised';
import uuid from 'uuid';
import util from 'util';
import { UberUtils, BadRequestError, NotFoundError } from '@badgeteam/service';
import { ProjectionTester } from '@badgeteam/service/testddd/projectionTester';
import { UserProjection } from '../code/userProjection';

let expect = chai.expect;
chai.use(chaiPromised);
chai.expect();

describe('UserProjection', () => {
    let userEvents1 = null;
    let userEvents2 = null;

    const createUserEvents = () => {
        const userId = uuid();
        const blockedUserId = uuid();
        const adminId = uuid();
        const badgeId = uuid();

        return {
            id: userId,
            blockedUserId,
            badgeId,
            adminId,
            created: {
                name: 'user.created',
                aggregateId: userId,
                eventData: {
                    nickname: `nickname ${uuid()}`,
                    avatar: `img-${uuid()}.jpg`
                }
            },
            deleted: {
                name: 'user.deleted',
                aggregateId: userId
            },
            banned: {
                name: 'user.banned',
                aggregateId: userId,
                adminId
            },
            nicknameChanged: {
                name: 'user.nicknameChanged',
                aggregateId: userId,
                eventData: {
                    nickname: `nickname ${uuid()}`
                }
            },
            avatarChanged: {
                name: 'user.avatarChanged',
                aggregateId: userId,
                eventData: {
                    avatar: `img-${uuid()}.jpg`
                }
            },
            userBlocked: {
                name: 'user.userBlocked',
                aggregateId: userId,
                eventData: {
                    target: 'badgeUnlock',
                    userId: blockedUserId,
                    badgeId
                }
            }
        };
    };

    const createProjection = (events)  => {
        return new ProjectionTester(UserProjection, events).make();
    };

    const fixStates = (states) => {
        _.forEach(states, (state) => {
            if (_.has(state, 'res_avatar'))
                UberUtils.rename(state, 'res_avatar', 'avatar');
        });
    };

    const findUser = (states, expectedId, expectedState) => {
        fixStates(states);
        return (_.findIndex(states, _.matches(_.merge({ id: expectedId }, expectedState))) >= 0);
    };

    const expectedUser = (states, expectedId, expectedState) => {
        expect(findUser(states, expectedId, expectedState), `missed user ${expectedId} state ${util.inspect(expectedState)}`).to.be.true;
    };

    const unexpectedUser = (states, expectedId, expectedState) => {
        expect(findUser(states, expectedId, expectedState), `unexpected user ${expectedId} state ${util.inspect(expectedState)}`).to.be.false;
    };

    beforeEach(() => {
        userEvents1 = createUserEvents();
        userEvents2 = createUserEvents();
    });

    describe('user.created', () => {
        it('single', () => {
            return createProjection([userEvents1.created])
                .then((tester) => tester.execQuery('user.get', { userId: userEvents1.id }))
                .then((result) => expectedUser([result], userEvents1.id,
                    _.merge({ isDeleted: false, isBanned: false }, userEvents1.created.eventData)));
        });

        it('skip avatar', () => {
            _.unset(userEvents1.created.eventData, 'avatar');
            return createProjection([userEvents1.created])
                .then((tester) => tester.execQuery('user.get', { userId: userEvents1.id }))
                .then((result) => expectedUser([result], userEvents1.id, userEvents1.created.eventData));
        });

        it('skip email', () => {
            _.unset(userEvents1.created.eventData, 'email');
            return createProjection([userEvents1.created])
                .then((tester) => tester.execQuery('user.get', { userId: userEvents1.id }))
                .then((result) => expectedUser([result], userEvents1.id, userEvents1.created.eventData));
        });
    });

    describe('user.deleted', () => {
        it('single', () => {
            return createProjection([userEvents1.created, userEvents1.deleted])
                .then((tester) => tester.execQuery('user.get', { userId: userEvents1.id }))
                .then((result) => expectedUser([result], userEvents1.id, {
                    nickname: 'Deleted User',
                    avatar: '[static]user_deleted.png',
                    isDeleted: true
                }));
        });
    });

    describe('user.banned', () => {
        it('single', () => {
            return createProjection([userEvents1.created, userEvents1.banned])
                .then((tester) => tester.execQuery('user.get', { userId: userEvents1.id }))
                .then((result) => expectedUser([result], userEvents1.id, {
                    nickname: 'Banned User',
                    avatar: '[static]user_deleted.png',
                    isDeleted: false,
                    isBanned: true
                }));
        });
    });

    describe('user.nicknameChanged', () => {
        it('single', () => {
            return createProjection([userEvents1.created, userEvents1.nicknameChanged])
                .then((tester) => tester.execQuery('user.get', { userId: userEvents1.id }))
                .then((result) => expectedUser([result], userEvents1.id, userEvents1.nicknameChanged.eventData));
        });

        it('BadRequest: no title', () => {
            return createProjection([userEvents1.created])
                .then((tester) => {
                    _.unset(userEvents1.nicknameChanged.eventData, 'nickname');
                    return expect(tester.sendEvents(userEvents1.nicknameChanged))
                        .to.be.rejectedWith(BadRequestError);
                });
        });
    });

    describe('user.avatarChanged', () => {
        it('single', () => {
            return createProjection([userEvents1.created, userEvents1.avatarChanged])
                .then((tester) => tester.execQuery('user.get', { userId: userEvents1.id }))
                .then((result) => expectedUser([result], userEvents1.id, userEvents1.avatarChanged.eventData));
        });

        it('BadRequest: no avatar', () => {
            return createProjection([userEvents1.created])
                .then((tester) => {
                    _.unset(userEvents1.avatarChanged.eventData, 'avatar');
                    return expect(tester.sendEvents(userEvents1.avatarChanged))
                        .to.be.rejectedWith(BadRequestError);
                });
        });
    });

    describe('user.get', () => {
        it('default', () => {
            return createProjection([
                    userEvents1.created,
                    userEvents2.created
                ])
                .then((tester) => tester.execQuery('user.get', { userId: userEvents1.id }))
                .then((result) => {
                    expectedUser([result], userEvents1.id);
                    unexpectedUser([result], userEvents2.id);
                });
        });
    });

    describe('user.getBlockedUsers', () => {
        it('default', () => {
            return createProjection([
                    userEvents1.created,
                    userEvents1.userBlocked
                ])
                .then((tester) => tester.execQuery('user.getBlockedUsers', { userId: userEvents1.id }))
                .then((result) => {
                    expect(result).to.be.eql({
                        users: [
                            { id: userEvents1.blockedUserId }
                        ]
                    });
                });
        });

        it('NotFound: user deleted', () => {
            return createProjection([
                    userEvents1.created,
                    userEvents1.userBlocked,
                    userEvents1.deleted
                ])
                .then((tester) => expect(tester.execQuery('user.getBlockedUsers', { userId: userEvents1.id }))
                    .to.be.rejectedWith(NotFoundError));
        });

        it('BadRequest: no userId within payload', () => {
            return createProjection([
                    userEvents1.created,
                    userEvents1.userBlocked
                ])
                .then((tester) => expect(tester.execQuery('user.getBlockedUsers', {  }))
                    .to.be.rejectedWith(BadRequestError));
        });
    });

    describe('query.search', () => {
        let tester = null;

        beforeEach(() => {
            return createProjection([
                    _.set(userEvents1.created, 'eventData.nickname', 'crafting'),
                    _.set(userEvents2.created, 'eventData.nickname', 'crafter')
                ])
                .then((t) => {
                    tester = t;
                });
        });

        it('default searches', () => {
            return tester.execQuery('user.search', { query: 'craft'})
                .then((result) => {
                    expectedUser(result, userEvents1.id);
                    expectedUser(result, userEvents2.id);
                    return tester.execQuery('user.search', { query: 'crafter'});
                })
                .then((result) => {
                    unexpectedUser(result, userEvents1.id);
                    expectedUser(result, userEvents2.id);
                    return tester.execQuery('user.search', { query: 'crafting'});
                })
                .then((result) => {
                    expectedUser(result, userEvents1.id);
                    unexpectedUser(result, userEvents2.id);
                    return tester.execQuery('user.search', { query: 'cRaFt'});
                })
                .then((result) => {
                    expectedUser(result, userEvents1.id);
                    expectedUser(result, userEvents2.id);
                });
        });

        it('search limit', () => {
            return tester.execQuery('user.search', { query: 'craft', limit: 1})
                .then((result) => {
                    unexpectedUser(result, userEvents1.id);
                    expectedUser(result, userEvents2.id);
                });
        });

        it('search limit & offset', () => {
            return tester.execQuery('user.search', { query: 'craft', limit: 1, offset: 1})
                .then((result) => {
                    expectedUser(result, userEvents1.id);
                    unexpectedUser(result, userEvents2.id);
                });
        });
    });
});


