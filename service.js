import Promise from 'bluebird';
import buildVersion from './buildVersion.json';
import { MicroService, Logger, BoundedContext, EventStore } from '@badgeteam/service';
import { MongoStateStore, BuildVersion as UberDbVersion } from '@badgeteam/db';

import { UserAggregate } from './code/userAggregate';
import { UserLocationAggregate } from './code/userLocationAggregate';
import { CommentAggregate } from './code/commentAggregate';
import { UserProjection } from './code/userProjection';
import { RestAccountProcessor } from './code/restAccountProcessor';
import { RestAdminProcessor } from './code/restAdminProcessor';
import { FeedProjection} from './feed/feedProjection';
import { BadgeUnlockCommentsProjection } from './code/badgeUnlockCommentsProjection';
import { RestFeedProcessor } from './feed/restFeedProcessor';
import { RestCommentsProcessor } from './code/restCommentsProcessor';

export default class UserManagerService extends MicroService {
    /**
     * @public
     * @constructor
     * @param {Object} config - Service config
     */
    constructor(config) {
        super(config);
        this.logger.info('uberdb library version: %s', UberDbVersion.buildVersion);
    }

    /**
     * @override
     */
    assertConfig(config) {
        super.assertConfig(config);

        if (!this.isFailed) {
            if (!this.checkConfigProperty(config, 'db.url'))
                return;

            this._dbConfig = config.db;
        }
    }

    /**
     * @override
     */
    startPromise() {
        this._stateStore = new MongoStateStore(
            this._dbConfig,
            new Logger('mongo-state-store'),
            (error) => this.unrecoverableFailure(error)
        );
        return this._stateStore.connect()
            .then(() => {
                return super.startPromise();
            });
    }

    /**
     * @override
     * @param {Object} query - Monitor query details
     * @returns {Object} - service status
     */
    onMonitorStatusQuery(query) {
        return super.onMonitorStatusQuery(query)
            .then((status) => {
                status.buildVersion = buildVersion.buildVersion;
                status.uberDbVersion = UberDbVersion.buildVersion;
                return status;
            });
    }

    /**
     * @override
     */
    initBoundedContexts() {
        this._eventStore = new EventStore(this.getDomain(MicroService.EventStoreDomainName));
        this._context = new BoundedContext(this._eventStore, this._stateStore, (name) => this.getDomain(name), 'user-manager-context');

        return Promise.all([
                this._context.bindAggregate(UserAggregate),
                this._context.bindAggregate(UserLocationAggregate),
                this._context.bindAggregate(CommentAggregate),
                this._context.bindProjection(UserProjection),
                this._context.bindProjection(FeedProjection),
                this._context.bindProjection(BadgeUnlockCommentsProjection)
            ])
            .then(() => {
                return this._context;
            });
    }

    /**
     * @override
     */
    initProcessors() {
        return super.initProcessors()
            .then(() => Promise.all([
                this.useDomainProcessor('com.onbadge.rest.account',
                    new RestAccountProcessor(this.getDomain(`com.onbadge.user`))),
                this.useDomainProcessor('com.onbadge.rest.admin',
                    new RestAdminProcessor(this.getDomain(`com.onbadge.user`))),
                this.useDomainProcessor('com.onbadge.rest.feed',
                    new RestFeedProcessor((name) => this.getDomain(`com.onbadge.${name}`))
                ),
                this.useDomainProcessor('com.onbadge.rest.comments',
                    new RestCommentsProcessor(
                        this.getDomain('com.onbadge.comment'),
                        this.getDomain('com.onbadge.badge')
                    )
                )
            ]));
    }

    /**
     * @override
     */
    stop(code) {
        this.logger.info('closing mongo state store');
        return this._stateStore.disconnect()
            .then(super.stop(code));
    }
}