import { EventProjectionComponent } from '@badgeteam/service';


export class BadgesProjectionComponent extends EventProjectionComponent {
    initialize() {
        this._store = this.createStore('badges');
    }

    getBadge(id) {
        return this._store.getState(id);
    }

    handleBadgeEventCreated({ aggregateId, eventData, date }) {
        const { creatorId, title='No Title', image='', description='No Description', cover={ color: '#1c88e3' } } = eventData;
        const badge = { title, res_image: image, description, cover };
        return this._store.setState(aggregateId, badge)
            .then(() => this.notifyChanged('created', aggregateId, { creatorId, date, ...badge }));
    }

    handleBadgeEventTitleChanged({ aggregateId, eventData }) {
        const { title='No Title' } = eventData;
        const update = { title };
        return this._store.patchState(aggregateId, { $set: update })
            .then(() => this.notifyChanged('updated', aggregateId, update));
    }

    handleBadgeEventDeleted({ aggregateId }) {
        return this._store.removeState(aggregateId)
            .then(() => this.notifyChanged('deleted', aggregateId));
    }

    handleBadgeEventImageChanged({ aggregateId, eventData }) {
        const { image='' } = eventData;
        const update = { res_image: image };
        return this._store.patchState(aggregateId, { $set: update })
            .then(() => this.notifyChanged('updated', aggregateId, update));
    }

    handleBadgeEventDescriptionChanged({ aggregateId, eventData }) {
        const { description='No Description' } = eventData;
        const update = { description };
        return this._store.patchState(aggregateId, { $set: update })
            .then(() => this.notifyChanged('updated', aggregateId, update));
    }

    handleBadgeEventCoverChanged({ aggregateId, eventData }) {
        const { cover={ color: '#1c88e3' } } = eventData;
        const update = { cover };
        return this._store.patchState(aggregateId, { $set: update })
            .then(() => this.notifyChanged('updated', aggregateId, update));
    }

    handleBadgeConditionsEventCreated({ eventData }) {
        const { badgeId, conditions={ manual: { enabled: true, flow: 'both' } } } = eventData;
        const update = { conditions };
        return this._store.patchState(badgeId, { $set: update })
            .then(() => this.notifyChanged('updated', badgeId, update));
    }

    handleBadgeConditionsEventChanged({ eventData }) {
        const { badgeId, conditions={ manual: { enabled: true, flow: 'both' } } } = eventData;
        const update = { conditions };
        return this._store.patchState(badgeId, { $set: update })
            .then(() => this.notifyChanged('updated', badgeId, update));
    }
}
