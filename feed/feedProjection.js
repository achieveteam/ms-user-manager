import _ from 'lodash';
import Promise from 'bluebird';
import Uuid from 'uuid';
import { EventProjection } from '@badgeteam/service';

import { UsersProjectionComponent } from '../code/usersProjectionComponent';
import { BadgesProjectionComponent } from './badgesProjectionComponent';
import { FollowersProjectionComponent } from './followersProjectionComponent';
import { LikesProjectionComponent } from './likesProjectionComponent';
import { HistoryProjectionComponent } from './historyProjectionComponent';

function prefixKeys(object, prefix) {
    return _.mapKeys(object, (value, key) => `${prefix}.${key}`);
}

export class FeedProjection extends EventProjection {
    static getComponents() {
        return [
            UsersProjectionComponent,
            BadgesProjectionComponent,
            FollowersProjectionComponent,
            LikesProjectionComponent,
            HistoryProjectionComponent
        ];
    }

    initialize() {
        this._store = this.createStore('feed', [
            { index: { 'userId': 1 } },
            { index: { 'user.id': 1 } },
            { index: { 'unlockData.admin.id': 1 } },
            { index: { 'user.id': 1, 'badge.id': 1 } }
        ]);
    }

    get users() { return this.getComponent(UsersProjectionComponent); }
    get followers() { return this.getComponent(FollowersProjectionComponent); }
    get likes() { return this.getComponent(LikesProjectionComponent); }
    get badges() { return this.getComponent(BadgesProjectionComponent); }
    get history() { return this.getComponent(HistoryProjectionComponent); }

    respondFeedQueryGet({ authUserId, userId, limit, offset, date, filter }) {
        const conditions = { userId };
        if (!_.isEmpty(date))
            _.merge(conditions, { date: { $lte: new Date(date) } });

        if (_.isEqual(filter, 'badgeUnlocked'))
            conditions[ 'type' ] = 'unlocked';
        else if (_.isEqual(filter, 'badgeCreated'))
            conditions[ 'type' ] = 'created';

        return Promise.join(
            this._store.getStates(conditions, { userId: 0 }, { date: -1 }, offset, limit),
            this.likes.getLikes(authUserId),
            (items, userLikes) => !items ? [] :
                items.map((item) => {
                    if (item.type !== 'unlocked')
                        return item;
                    const { likes, comments, unlockData, ...props } = item;
                    return {
                        ...props,
                        unlockData: {
                            ...unlockData,
                            comments,
                            likes: {
                                value: likes,
                                liked: !_.isNil(userLikes.find(({ badgeId, unlockerId }) =>
                                    badgeId === item.badge.id && unlockerId === item.user.id)
                                )
                            }
                        }
                    };
                })
        );
    }

    componentChanged(name, key, state, component) {
        switch (component.constructor.name) {
            case BadgesProjectionComponent.name:
                return this.badgeChanged(name, key, state);
            case UsersProjectionComponent.name:
                return this.userChanged(name, key, state);
            case LikesProjectionComponent.name:
                return this.likeChanged(name, key, state);
            case HistoryProjectionComponent.name:
                return this.historyChanged(name, state);
        }
    }

    badgeChanged(type, id, change) {
        switch (type) {
            case 'created': {
                const { creatorId, date } = change;
                return this.history.created(id, creatorId, date);
            }
            case 'updated': {
                return this._store.patchStates(prefixKeys({ id }, 'badge'), { $set: prefixKeys(change, 'badge') });
            }
            case 'deleted': {
                return this._store.removeStates({ 'badge.id': id });
            }
        }
    }

    userChanged(type, id, change) {
        switch (type) {
            case 'updated':
                return Promise.map(['user', 'unlockData.admin'], (prefix) =>
                    this._store.patchStates(prefixKeys({ id }, prefix), { $set: prefixKeys(change, prefix) })
                );
            case 'deleted':
            case 'banned':
                return this._store.removeStates({ 'user.id': id });
        }
    }

    likeChanged(type, id, change) {
        switch (type) {
            case 'changed': {
                const { unlockerId, badgeId, value } = change;
                return this._store.patchStates({ 'badge.id': badgeId, 'user.id': unlockerId, type: 'unlocked' },
                    { $inc: { likes: value }}
                );
            }
        }
    }

    historyChanged(type, change) {
        switch (type) {
            case 'created': {
                const { userId, badgeId, date } = change;
                return Promise.join(
                    this.users.getUser(userId),
                    this.badges.getBadge(badgeId),
                    this.followers.getFollowers(userId),
                    (user, badge, followerIds) =>
                        Promise.map(followerIds.concat(null, userId), (followerId) =>
                            this._store.setState(Uuid(), {
                                userId: followerId, date, type,
                                user: _.merge({ id: userId }, user),
                                badge: _.merge({ id: badgeId }, badge)
                            })
                        )
                );
            }
            case 'unlocked':
                const { userId, badgeId, unlockData: { adminId, ...props }, date } = change;
                return Promise.join(
                    this.users.getUser(userId),
                    this.users.getUser(adminId),
                    this.badges.getBadge(badgeId),
                    this.followers.getFollowers(userId),
                    (user, admin, badge, followerIds) =>
                        Promise.map(followerIds.concat(null, userId), (followerId) =>
                            this._store.setState(Uuid(), {
                                userId: followerId, date, type,
                                user: _.merge({ id: userId }, user),
                                badge: _.merge({ id: badgeId }, badge),
                                unlockData: {
                                    ...props,
                                    admin: _.merge({ id: adminId }, admin)
                                },
                                comments: 0, likes: 0
                            })
                        )
                );
        }
    }

    handleCommentEventBadgeUnlockCommented({ eventData }) {
        const { badgeId, unlockerId } = eventData;
        return this._store.patchStates({ 'badge.id': badgeId, 'user.id': unlockerId, type: 'unlocked' },
            { $inc: { comments: +1 } }
        );
    }

    handleCommentEventBadgeUnlockCommentDeleted({ eventData }) {
        const { badgeId, unlockerId } = eventData;
        return this._store.patchStates({ 'badge.id': badgeId, 'user.id': unlockerId, type: 'unlocked' },
            { $inc: { comments: -1 } }
        );
    }
}