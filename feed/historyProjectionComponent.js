import Uuid from 'uuid';

import { EventProjectionComponent } from '@badgeteam/service';


export class HistoryProjectionComponent extends EventProjectionComponent {
    initialize() {
        this._store = this.createStore('history');
    }

    created(badgeId, userId, date) {
        const type = 'created';
        const payload = { badgeId, userId, date: new Date(date) };
        return this._store.setState(Uuid(), { type, ...payload })
            .then(() => this.notifyChanged(type, null, payload));
    }

    unlocked(badgeId, userId, unlockData, date) {
        const type = 'unlocked';
        const { evidence={}, ...props } = unlockData;
        const { title='No Title', image='' } = evidence;
        const payload = {
            badgeId, userId, date: new Date(date),
            unlockData: { ...props, evidence: { title, res_image: image } },
        };
        return this._store.setState(Uuid(), { type, ...payload })
            .then(() => this.notifyChanged(type, null, payload));
    }

    handleUserBadgesEventUnlocked({ eventData, date }) {
        const { userId, badgeId, match: { unlockData={} } } = eventData;
        return this.unlocked(badgeId, userId, unlockData, date);
    }
}
