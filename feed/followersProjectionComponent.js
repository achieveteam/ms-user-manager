import _ from 'lodash';
import { EventProjectionComponent } from '@badgeteam/service';


export class FollowersProjectionComponent extends EventProjectionComponent {
    initialize() {
        this._store = this.createStore('followers');
    }

    getFollowers(userId) {
        return this._store.getState(userId)
            .then(({ followerIds=[] }) => followerIds);
    }

    handleUserFriendsEventAdded({ eventData }) {
        const { userId, friendIds=[] } = eventData;
        return this._store.getState(userId)
            .then(({ followerIds=[] }) =>
                this._store.setState(userId, { followerIds: _.union(followerIds, friendIds) })
            );
    }

    handleUserFriendsEventRemoved({ eventData }) {
        const { userId, friendIds=[] } = eventData;
        return this._store.getState(userId)
            .then(({ followerIds=[] }) =>
                this._store.setState(userId, { followerIds: _.difference(followerIds, friendIds) })
            );
    }
}
