import _ from 'lodash';
import Promise from 'bluebird';
import Uuid from 'uuid';

import { EventProjectionComponent } from '@badgeteam/service';


export class LikesProjectionComponent extends EventProjectionComponent {
    initialize() {
        this._store = this.createStore('likes', [
            { index: { 'userId': 1 } },
            { index: { 'user.id': 1, 'badge.id': 1 } }
        ]);
    }

    getLikes(userId) {
        return this._store.getState(userId)
            .then(({ likes=[] }) => likes);
    }

    handleUserBadgesEventUnlockedBadgeLiked({ eventData }) {
        const { userId, badgeId, fellowId } = eventData;
        const like = { badgeId, unlockerId: userId };
        return this._store.getState(fellowId)
            .then(({ likes=[] }) => {
                if (likes.find((value) => _.isEqual(value, like)))
                    return;
                return this._store.setState(fellowId, { likes: _.unionWith(likes, [like], _.isEqual) })
                    .then(() => this.notifyChanged('changed', null, { userId: fellowId, value: 1, ...like }));
            });
    }

    handleUserBadgesEventUnlockedBadgeDisliked({ eventData }) {
        const { userId, badgeId, fellowId } = eventData;
        const like = { badgeId, unlockerId: userId };
        return this._store.getState(fellowId)
            .then(({ likes=[] }) => {
                if (!likes.find((value) => _.isEqual(value, like)))
                    return;
                return this._store.setState(fellowId, { likes: _.differenceWith(likes, [like], _.isEqual) })
                    .then(() => this.notifyChanged('changed', null, { userId: fellowId, value: -1, ...like }));
            });
    }
}
