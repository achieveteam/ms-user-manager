import _ from 'lodash';
import Promise from 'bluebird';
import { Processor, UnauthorizedError } from '@badgeteam/service';


export class RestFeedProcessor extends Processor {
    constructor(domain) {
        super();
        this._domain = domain;
    }

    processQueryGet(payload) {
        const authorized = _.get(payload, 'auth.authorized', false);

        return this.pick(payload, 'auth', '#type', '#_.toInteger(offset)', '#_.toInteger(limit)', '#date', '#filter')
            .then(({ auth, type = 'user', offset = 0, limit = 20, date, filter = '' }) => {
                if (!authorized) {
                    offset = 0;
                    limit = 3;
                    filter = 'badgeUnlocked';
                    type = 'global';
                }

                return this._domain('feed').query('get', {
                    userId: type === 'global' ? null : auth.userId,
                    authUserId: auth.userId, offset, limit, date,
                    filter
                })
            });
    }
}