import _ from 'lodash';
import Promise from 'bluebird';
import joi from 'joi';
import { EventProjection, BadRequestError, NotFoundError } from '@badgeteam/service';
import { UsersProjectionComponent } from './usersProjectionComponent';

export class BadgeUnlockCommentsProjection extends EventProjection {
    static getComponents() {
        return [
            UsersProjectionComponent
        ];
    }

    /** @returns {UserProjectionComponent} */
    get users() { return this.getComponent(UsersProjectionComponent); }

    /**
     * @private
     * @return Promise
     */
    validatePayload(payload, scheme) {
        const result = joi.validate(payload, scheme, { stripUnknown: true });
        if (result.error)
            return BadRequestError.promise(result.error.message);
        return Promise.resolve(result.value);
    }

    initialize() {
        this._commentStore = this.createStore('comment', [
            { index: { badgeId: 1, unlockerId: 1 } },
            { index: { 'user.id': 1 } }
        ]);
    }


    componentChanged(name, key, state, component) {
        switch (component) {
            case this.users:
                return this.userChanged(name, key, state);
        }
    }

    userChanged(type, id, change) {
        switch (type) {
            case 'updated':
                return this._commentStore.getStates({ 'user.id': id })
                    .then((items) => Promise.map(items, ({ id, user, ...props }) =>
                        this._commentStore.saveState(id, { user: _.merge(user, change), ...props })
                    ));

            case 'deleted':
                return this._commentStore.getStates({ 'user.id': id })
                    .then((items) => Promise.map(items, ({ id }) =>
                        this._commentStore.removeState(id)
                    ));
        }
    }

    handleCommentEventBadgeUnlockCommented(event) {
        const { userId, badgeId, unlockerId, content } = event.eventData;
        const date = new Date(event.date).toJSON();

        return this.users.getUser(userId)
            .then(user => this._commentStore.saveState(event.aggregateId, {
                user, badgeId, unlockerId, content,
                creationDate: date,
                editDate: date
            }));
    }

    handleCommentEventBadgeUnlockCommentEdited(event) {
        const { content } = event.eventData;
        return this._commentStore.updateState(event.aggregateId, {
            editDate: new Date(event.date).toJSON(),
            content
        });
    }

    handleCommentEventBadgeUnlockCommentDeleted(event) {
        return this._commentStore.removeState(event.aggregateId);
    }

    static GetBadgeUnlockCommentsScheme = joi.object().keys({
        badgeId: joi.string().guid({ version: 'uuidv4' }).when('target', { is: 'badgeUnlock', then: joi.required() }),
        unlockerId: joi.string().guid({ version: 'uuidv4' }).when('target', { is: 'badgeUnlock', then: joi.required() }),
    });
    respondCommentQueryGetBadgeUnlockComments(payload) {
        return this.validatePayload(payload, this.constructor.GetBadgeUnlockCommentsScheme)
            .then(({ badgeId, unlockerId }) => this._commentStore.getStates(
                { badgeId, unlockerId },
                { badgeId: 0, unlockerId: 0 },
                { creationDate: 1 }
            ));
    }
}
