import _ from 'lodash';
import joi from 'joi';
import Promise from 'bluebird';
import { Aggregate, BadRequestError, ConflictError, NotFoundError } from '@badgeteam/service';

export class CommentAggregate extends Aggregate {
    static Scope = 'comment';

    initialize() {
        this._exists = false;
    }

    validatePayload(payload, scheme) {
        const result = joi.validate(payload, scheme, { stripUnknown: true });
        if (result.error)
            return BadRequestError.promise(result.error.message);
        return Promise.resolve(result.value);
    }

    static CommentBadgeUnlockScheme = joi.object().keys({
        userId: joi.string().guid({ version: 'uuidv4' }).required(),
        unlockerId: joi.string().guid({ version: 'uuidv4' }).required(),
        badgeId: joi.string().guid({ version: 'uuidv4' }).required(),
        content: joi.string().max(256).required()
    });
    commandCommentBadgeUnlock(payload) {
        if (this._exists)
            return ConflictError.promise('comment already exists');

        return this.validatePayload(payload, this.constructor.CommentBadgeUnlockScheme)
            .then(params => this.emitEvent('badgeUnlockCommented', params));
    }

    static EditScheme = joi.object().keys({
        userId: joi.string().guid({ version: 'uuidv4' }).required(),
        content: joi.string().max(256).required()
    });
    commandEdit(payload) {
        if (!this._exists)
            return NotFoundError.promise('comment not found');

        return this.validatePayload(payload, this.constructor.EditScheme)
            .then(params => {
                if (!_.isEqual(params.userId, this._userId))
                    return BadRequestError.promise('user is not an owner of the comment');

                if (!_.isEqual(params.content, this._content)) {
                    params.unlockerId = this._unlockerId;
                    params.badgeId = this._badgeId;
                    this.emitEvent('badgeUnlockCommentEdited', params)
                }
            });
    }

    static DeleteScheme = joi.object().keys({
        userId: joi.string().guid({ version: 'uuidv4' }).required()
    });
    commandDelete(payload) {
        if (!this._exists)
            return NotFoundError.promise('comment not found');

        return this.validatePayload(payload, this.constructor.DeleteScheme)
            .then(params => {
                if (!_.isEqual(params.userId, this._userId))
                    return BadRequestError.promise('user is not an owner of the comment');

                params.badgeId = this._badgeId;
                params.unlockerId = this._unlockerId;

                this.emitEvent('badgeUnlockCommentDeleted', params)
            });
    }

    handleBadgeUnlockCommented(data) {
        this._exists = true;
        this._userId = data.userId;
        this._unlockerId = data.unlockerId;
        this._badgeId = data.badgeId;
        this._content = data.content;
    }

    handleBadgeUnlockCommentDeleted() {
        this._exists = false;
    }
}