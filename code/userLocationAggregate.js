import joi from 'joi';
import { Aggregate, BadRequestError } from '@badgeteam/service';

const locationShema = joi.object().keys({
    latitude: joi.number().required(),
    longitude: joi.number().required()
});

export class UserLocationAggregate extends Aggregate {
    static Scope = 'userLocation';

    initialize() {
        this._exists = false;
    }

    commandDiscover(payload) {
        return this.pick(payload, 'userId', 'badgeId', 'location', 'evidence')
            .then(({ userId, badgeId, location, evidence }) => {
                const { error, value } = joi.validate(location, locationShema, { stripUnknown: true });
                if (error)
                    return BadRequestError.promise(error.message);
                if (!this._exists)
                    this.emitEvent('created', { userId });
                this.emitEvent('discovered', { userId, badgeId, evidence, location: value });
            });
    }

    handleCreated() {
        this._exists = true;
    }
}
