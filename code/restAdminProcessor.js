import _ from 'lodash';
import joi from 'joi';
import Promise from 'bluebird';
import { Processor, UnauthorizedError, ServiceUnavailableError, BadRequestError } from '@badgeteam/service';

export class RestAdminProcessor extends Processor {
    /**
     * @constructor
     * @param {Domain} userAggregateDomain
     */
    constructor(userAggregateDomain) {
        super();
        this._userDomain = userAggregateDomain;
    }

    /**
     * @private
     * @return Promise
     */
    validatePayload(payload, scheme) {
        const result = joi.validate(payload, scheme, { stripUnknown: true });
        if (result.error)
            return BadRequestError.promise(result.error.message);
        return Promise.resolve(result.value);
    }

    /**
     * @private
     * @returns Promise
     */
    checkAuth(payload) {
        if (!_.get(payload, 'auth.authorized', false))
            return UnauthorizedError.promise('user not authorized');

        const permissions = _.get(payload, 'auth.permissions', []);

        if (!_.includes(permissions, 'admin'))
            // this should be 403 Forbidden
            return ServiceUnavailableError.promise('wrong permissions');

        return Promise.resolve();
    }

    /** admin.banUser */
    static BanUserPayload = joi.object().keys({
        userId: joi.string().guid({ version: 'uuidv4' }).required(),
        auth: joi.object().keys({
            userId: joi.string().guid({ version: 'uuidv4' })
        })
    });
    processCommandBanUser(payload) {
        return this.checkAuth(payload)
            .then(() => this.validatePayload(payload, this.constructor.BanUserPayload))
            .then(({ userId: aggregateId, auth: { userId: adminId }}) =>
                this._userDomain.command('banUser', { aggregateId, adminId })
            );
    }
}