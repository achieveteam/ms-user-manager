import Promise from 'bluebird';
import _ from 'lodash';
import { EventProjection, BadRequestError, NotFoundError } from '@badgeteam/service';

export class UserProjection extends EventProjection {
    initialize() {
        this._userStore = this.createStore('user', [ { index: { nickname: 1 }} ]);
        this._blockedUsersStore = this.createStore('blockedUsers');
    }

    handleUserEventCreated(event) {
        return this.pick(event.eventData, 'nickname', '#avatar=>res_avatar')
            .then((data) =>
                Promise.join(
                    this._userStore.saveState(event.aggregateId,
                        _.merge(data, { isDeleted: false, isBanned: false } )
                    ),
                    this._blockedUsersStore.saveState(event.aggregateId, { users: [] })
                )
            )
            .catch(BadRequestError, (error) => this.logger.warn(`user.created: ${error.message}`));
    }

    handleUserEventDeleted(event) {
        return Promise.join(
            this._userStore.saveState(event.aggregateId, {
                isDeleted: true,
                isBanned: false,
                nickname: 'Deleted User',
                res_avatar: '[static]user_deleted.png'
            }),
            this._blockedUsersStore.removeState(event.aggregateId)
        );
    }

    handleUserEventBanned(event) {
        return Promise.join(
            this._userStore.saveState(event.aggregateId, {
                isDeleted: false,
                isBanned: true,
                nickname: 'Banned User',
                res_avatar: '[static]user_deleted.png'
            }),
            this._blockedUsersStore.removeState(event.aggregateId)
        );
    }

    handleUserEventNicknameChanged(event) {
        return this.pick(event.eventData, 'nickname')
            .then((data) => {
                return this._userStore.updateState(event.aggregateId, data);
            });
    }

    handleUserEventAvatarChanged(event) {
        return this.pick(event.eventData, 'avatar=>res_avatar')
            .then((data) => {
                return this._userStore.updateState(event.aggregateId, data);
            });
    }

    handleUserEventUserBlocked(event) {
        const { aggregateId, eventData: { userId } } = event;

        if (!_.isEmpty(userId))
            return this._blockedUsersStore.loadState(aggregateId)
                .then((state) => {
                    const blockedUsers = _.get(state, 'users', []);
                    blockedUsers.push({ id: userId });
                    _.set(state, 'users', blockedUsers);
                    return this._blockedUsersStore.saveState(aggregateId, state);
                });
    }

    respondUserQueryGet(payload) {
        return this.pick(payload, 'userId')
            .then((params) => this._userStore.loadState(params.userId))
            .then(profile => {
                if (_.isEmpty(_.omit(profile, 'id')))
                    return NotFoundError.promise(`user ${profile.id} not found`);
                return profile;
            })
    }

    respondUserQuerySearch(payload) {
        return this.pick(payload, 'query', '#limit', '#offset')
            .then((params) => {
                return this._userStore.getStates(
                    { nickname: { $regex: new RegExp(`.*${params.query}.*`, 'i') } },
                    undefined,
                    { nickname: 1 },
                    _.get(params, 'offset', 0),
                    _.get(params, 'limit', 20)
                );
            });
    }

    respondUserQueryGetBlockedUsers(payload) {
        return this.pick(payload, 'userId')
            .then((params) => this._blockedUsersStore.loadState(params.userId))
            .then((result) => {
                const id = result.id;
                result = _.omit(result, 'id');
                if (_.isEmpty(result))
                    return NotFoundError.promise(`user ${id} not found`);
                return result;
            });
    }
}
