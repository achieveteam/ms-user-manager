import _ from 'lodash';
import Promise from 'bluebird';
import joi from 'joi';
import uuid from 'uuid';
import { Processor, UnauthorizedError, BadRequestError } from '@badgeteam/service';

export class RestCommentsProcessor extends Processor {
    /**
     * @constructor
     * @param {Domain} commentAggregateDomain
     */
    constructor(commentAggregateDomain, badgeDomain) {
        super();
        this._commentDomain = commentAggregateDomain;
        this._badgeDomain = badgeDomain;
    }

    /**
     * @private
     * @returns Promise
     */
    checkAuth(payload) {
        if (!_.get(payload, 'auth.authorized', false))
            return UnauthorizedError.promise('user not authorized');
        return Promise.resolve();
    }

    /**
     * @private
     * @return Promise
     */
    validatePayload(payload, scheme) {
        const result = joi.validate(payload, scheme, { stripUnknown: true });
        if (result.error)
            return BadRequestError.promise(result.error.message);
        return Promise.resolve(result.value);
    }

    /**
     * @private
     * @return Promise
     */
    validateBadgeUnlock(params) {
        const { badgeId, unlockerId: userId } = params;
        return this._badgeDomain.query('getById', { badgeId, userId })
            .then(badge => {
                if (_.get(badge, 'isLocked', true))
                    return BadRequestError.promise(`badge ${badgeId} is locked or not available for the user ${userId}`);
                return params;
            });
    }

    /** comments.comment */
    static CommentPayload = joi.object().keys({
        target: joi.string().valid(['badgeUnlock']).required(),
        badgeId: joi.string().guid({ version: 'uuidv4' }).when('target', { is: 'badgeUnlock', then: joi.required() }),
        unlockerId: joi.string().guid({ version: 'uuidv4' }).when('target', { is: 'badgeUnlock', then: joi.required() }),
        content: joi.string().max(256).required(),
        auth: joi.object().keys({
            userId: joi.string().guid({ version: 'uuidv4' })
        })
    });
    processCommandComment(payload) {
        return this.checkAuth(payload)
            .then(() => this.validatePayload(payload, this.constructor.CommentPayload))
            .then(params => this.validateBadgeUnlock(params))
            .then(params => {
                params['userId'] = params.auth.userId;
                params['aggregateId'] = uuid();
                _.unset(params, 'auth');
                _.unset(params, 'target');
                return this._commentDomain.command('commentBadgeUnlock', params)
                    .then(() => ({ id: params.aggregateId }));
            });
    }

    /** comments.edit */
    static EditPayload = joi.object().keys({
        commentId: joi.string().guid({ version: 'uuidv4' }).required(),
        content: joi.string().max(256).required(),
        auth: joi.object().keys({
            userId: joi.string().guid({ version: 'uuidv4' })
        })
    });
    processCommandEdit(payload) {
        return this.checkAuth(payload)
            .then(() => this.validatePayload(payload, this.constructor.EditPayload))
            .then(params => {
                params['userId'] = params.auth.userId;
                params['aggregateId'] = params.commentId;
                _.unset(params, 'auth');
                _.unset(params, 'commentId');
                return this._commentDomain.command('edit', params);
            });
    }

    /** comments.edit */
    static DeletePayload = joi.object().keys({
        commentId: joi.string().guid({ version: 'uuidv4' }).required(),
        auth: joi.object().keys({
            userId: joi.string().guid({ version: 'uuidv4' })
        })
    });
    processCommandDelete(payload) {
        return this.checkAuth(payload)
            .then(() => this.validatePayload(payload, this.constructor.DeletePayload))
            .then(params => {
                params['userId'] = params.auth.userId;
                params['aggregateId'] = params.commentId;
                _.unset(params, 'auth');
                _.unset(params, 'commentId');
                return this._commentDomain.command('delete', params);
            });
    }

    /** comments.get */
    static GetPayload = joi.object().keys({
        target: joi.string().valid(['badgeUnlock']).required(),
        badgeId: joi.string().guid({ version: 'uuidv4' }).when('target', { is: 'badgeUnlock', then: joi.required() }),
        unlockerId: joi.string().guid({ version: 'uuidv4' }).when('target', { is: 'badgeUnlock', then: joi.required() }),
        auth: joi.object().keys({
            userId: joi.string().guid({ version: 'uuidv4' })
        })
    });
    processQueryGet(payload) {
        return this.checkAuth(payload)
            .then(() => this.validatePayload(payload, this.constructor.GetPayload))
            .then(params => {
                params['userId'] = params.auth.userId;
                _.unset(params, 'auth');
                _.unset(params, 'target');
                return this._commentDomain.query('getBadgeUnlockComments', params);
            });
    }
}