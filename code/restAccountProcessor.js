import _ from 'lodash';
import joi from 'joi';
import Promise from 'bluebird';
import { Processor, UnauthorizedError, BadRequestError } from '@badgeteam/service';

export class RestAccountProcessor extends Processor {
    /**
     * @constructor
     * @param {Domain} userAggregateDomain
     */
    constructor(userAggregateDomain) {
        super();
        this._userDomain = userAggregateDomain;
    }

    /**
     * @private
     * @return Promise
     */
    validatePayload(payload, scheme) {
        const result = joi.validate(payload, scheme, { stripUnknown: true });
        if (result.error)
            return BadRequestError.promise(result.error.message);
        return Promise.resolve(result.value);
    }

    /**
     * @private
     * @returns Promise
     */
    checkAuth(payload) {
        if (!_.get(payload, 'auth.authorized', false))
            return UnauthorizedError.promise('user not authorized');
        return Promise.resolve();
    }

    /** account.edit */
    processCommandEdit(payload) {
        this.logger.debug('handling command (account.edit)');

        return this.checkAuth(payload)
            .then(() => this.pick(payload, 'auth.userId=>aggregateId', '#nickname', '#avatar'))
            .then((data) => this._userDomain.command('update', data));
    }

    /** account.delete */
    processCommandDelete(payload) {
        this.logger.debug('handling command (account.delete)');

        return this.checkAuth(payload)
            .then(() => this.pick(payload, 'auth.userId=>aggregateId'))
            .then((data) => this._userDomain.command('delete', data));
    }

    /** account.blockUser */
    static BlockUserPayload = joi.object().keys({
        target: joi.string().valid(['badgeUnlock', 'badge']).required(),
        userId: joi.string().guid({ version: 'uuidv4' }).required(),
        badgeId: joi.string().guid({ version: 'uuidv4' }).required(),
        auth: joi.object().keys({
            userId: joi.string().guid({ version: 'uuidv4' })
        })
    });
    processCommandBlockUser(payload) {
        return this.checkAuth(payload)
            .then(() => this.validatePayload(payload, this.constructor.BlockUserPayload))
            .then((params) => {
                _.set(params, 'aggregateId', params.auth.userId);
                _.unset(params, 'auth');
                return this._userDomain.command('blockUser', params);
            });
    }

    /** account.get */
    processQueryGet(payload) {
        this.logger.debug('handling query (account.get)');

        return this.checkAuth(payload)
            .then(() => this.pick(payload, '#auth.userId=>currentUserId', '#userId'))
            .then((data) => this._userDomain.query('get', {
                userId: _.get(data, 'userId', _.get(data, 'currentUserId'))
            }));
    }

    /** account.getBlockedUsers */
    static GetBlockedUsersPayload = joi.object().keys({
        auth: joi.object().keys({
            userId: joi.string().guid({ version: 'uuidv4' })
        })
    });
    processQueryGetBlockedUsers(payload) {
        this.logger.debug('QUERY (account.getBlockedUsers)');

        return this.checkAuth(payload)
            .then(() => this.validatePayload(payload, this.constructor.GetBlockedUsersPayload))
            .then((params) => {
                return this._userDomain.query('getBlockedUsers', { userId: params.auth.userId });
            });
    }
}