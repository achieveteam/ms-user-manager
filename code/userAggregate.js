import _ from 'lodash';
import joi from 'joi';
import { Aggregate, NotFoundError, ConflictError, BadRequestError } from '@badgeteam/service';

export class UserAggregate extends Aggregate {
    static Scope = 'user';

    initialize() {
        this._exists = false;
        this._banned = false;
        this._blockedUsers = [];
    }

    validatePayload(payload, scheme) {
        const result = joi.validate(payload, scheme, { stripUnknown: true });
        if (result.error)
            return BadRequestError.promise(result.error.message);
        return Promise.resolve(result.value);
    }

    commandCreate(payload) {
        if (this._exists)
            return ConflictError.promise('user already exists');

        return this.pick(payload, 'nickname', '#avatar', '#email')
            .then((data) => {
                this.emitEvent('created', data);
            })
    }

    commandDelete() {
        if (!this._exists)
            return NotFoundError.promise(`user ${this.id} not exist`);

        return this.emitEvent('deleted');
    }

    commandUpdate(data) {
        if (!this._exists)
            return NotFoundError.promise(`user ${this.id} not exist`);

        if (!_.isEmpty(data.nickname) && !_.isEqual(this._nickname, data.nickname))
            this.emitEvent('nicknameChanged', { nickname: data.nickname });

        if (!_.isEmpty(data.email) && !_.isEqual(this._email, data.email))
            this.emitEvent('emailChanged', { email: data.email });

        if (!_.isEmpty(data.avatar) && !_.isEqual(this._avatar, data.avatar))
            this.emitEvent('avatarChanged', { avatar: data.avatar });
    }

    commandBindFriendsGroup(payload) {
        if (!this._exists)
            return NotFoundError.promise(`user ${this.id} not exist`);

        return this.pick(payload, 'groupId')
            .then(data => {
                if (!_.isEqual(this._friendsGroupId, data.groupId)) {
                    if (!_.isEmpty(this._friendsGroupId))
                        return ConflictError.promise(`user ${this.id} already has bounded friends group ${this._friendsGroupId}`, {
                            groupId: this._friendsGroupId
                        });

                    this.emitEvent('friendsGroupBounded', data);
                }
            });
    }

    static BlockUserScheme = joi.object().keys({
        target: joi.string().required(),
        userId: joi.string().guid({ version: 'uuidv4' }).required(),
        badgeId: joi.string().guid({ version: 'uuidv4' }).required()
    });
    commandBlockUser(payload) {
        if (!this._exists)
            return NotFoundError.promise('user not exists');

        return this.validatePayload(payload, this.constructor.BlockUserScheme)
            .then(params => {
                if (_.isEqual(params.userId, this.id))
                    return BadRequestError.promise('user can\'t block itself');

                if (!_.includes(this._blockedUsers, params.userId))
                    this.emitEvent('userBlocked', params)
            });
    }

    static BanUserScheme = joi.object().keys({
        adminId: joi.string().guid({ version: 'uuidv4' }).required()
    });
    commandBanUser(payload) {
        if (!this._exists)
            return NotFoundError.promise('user not exists');

        return this.validatePayload(payload, this.constructor.BanUserScheme)
            .then(params => {
                if (_.isEqual(params.adminId, this.id))
                    return BadRequestError.promise('user can\'t ban itself');

                if (!this._banned)
                    this.emitEvent('banned', params)
            });
    }

    handleCreated(data) {
        this._exists = true;
        this._nickname = _.get(data, 'nickname', '');
        this._email = _.get(data, 'email', '');
        this._avatar = _.get(data, 'avatar', '');
    }

    handleFriendsGroupBounded(data) {
        this._friendsGroupId = data.groupId;
    }

    handleDeleted() {
        this._exists = false;
    }

    handleBanned() {
        this._banned = true;
    }

    handleNicknameChanged(data) {
        this._nickname = _.get(data, 'nickname', this._nickname);
    }

    handleEmailChanged(data) {
        this._email = _.get(data, 'email', this._email);
    }

    handleAvatarChanged(data) {
        this._avatar = _.get(data, 'avatar', this._avatar);
    }

    handleUserBlocked(data) {
        this._blockedUsers.push(data.userId);
    }
}
