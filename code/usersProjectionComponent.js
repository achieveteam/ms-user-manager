import { EventProjectionComponent } from '@badgeteam/service';


export class UsersProjectionComponent extends EventProjectionComponent {
    initialize() {
        this._store = this.createStore('users');
    }

    getUser(id) {
        return this._store.loadState(id);
    }

    handleUserEventCreated({ aggregateId, eventData }) {
        const { nickname='No Name', avatar='' } = eventData;
        const create = { nickname, res_avatar: avatar };
        return this._store.saveState(aggregateId, create)
            .then(() => this.notifyChanged('created', aggregateId, create));
    }

    handleUserEventNicknameChanged({ aggregateId, eventData}) {
        const { nickname='No Name' } = eventData;
        const update = { nickname };
        return this._store.updateState(aggregateId, update)
            .then(() => this.notifyChanged('updated', aggregateId, update));
    }

    handleUserEventAvatarChanged({ aggregateId, eventData }) {
        const { avatar='' } = eventData;
        const update = { res_avatar: avatar };
        return this._store.updateState(aggregateId, update)
            .then(() => this.notifyChanged('updated', aggregateId, update));
    }

    handleUserEventDeleted({ aggregateId }) {
        return this._store.removeState(aggregateId)
            .then(() => this.notifyChanged('deleted', aggregateId));
    }

    handleUserEventBanned({ aggregateId }) {
        return this._store.removeState(aggregateId)
            .then(() => this.notifyChanged('banned', aggregateId));
    }
}
