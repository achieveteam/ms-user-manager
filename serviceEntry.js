require('./babelHook');

var config = require('./config').default;
var Service = require('./service').default;

(new Service(config)).start();
