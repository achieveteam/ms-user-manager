#!/bin/bash

THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo "Requesting secret environment from aws secrets bucket"

SECRET_ENV=$(bash ${THIS}/aws-tools/get-s3-secret .env)

eval ${SECRET_ENV}

echo "Secret environment exported, launch node process"

export NODE_ENV=production
export NODE_DISABLE_COLORS=1
export DEBUG_COLORS=0
export DEBUG_FD=0

node ${THIS}/serviceEntry.js
