export default {
    microservice : {
        name: 'user-manager',
        debugName: 'user-manager'
    },
    bus: {
        debugName: 'user-manager-bus',
        url: 'amqp://message-bus',
        socketOptions: {}
    },
    db: {
        url: 'mongodb://mongo-database/user-manager',
        reconnectInterval: 1000
    }
};