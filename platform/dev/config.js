export default {
    microservice : {
        name: 'user-manager',
        debugName: 'user-manager'
    },
    bus: {
        debugName: 'user-manager-bus',
        url: 'amqp://%env.DOCKER_HOST_IP%',
        socketOptions: {}
    },
    remoteLogger: {
        token: '8AD2D211C6C4493D91C8DB3EF66B8310'
    },
    db: {
        url: 'mongodb://%env.DOCKER_HOST_IP%/user-manager',
        reconnectInterval: 1000
    }
};