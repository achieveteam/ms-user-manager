export default {
    microservice : {
        name: 'user-manager',
        debugName: 'user-manager'
    },
    bus: {
        debugName: 'user-manager-bus',
        url: 'amqp://localhost',
        socketOptions: {}
    },
    db: {
        url: 'mongodb://localhost/onbadge',
        reconnectInterval: 1000
    }
};